#!/bin/sh

#SBATCH --export=NONE
#SBATCH --mem=50G
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --job-name=comparative_genomics
#TODO adjust time scheduler, currently "days-hours:minutes:seconds"
#SBATCH --time=7-00:00:00


export LC_ALL="C"
BIND=/data

# Commands
#singularity exec --bind <data_path if not run from /tmp or /home> /software/singularity/containers/Projects/SPSP
SINGULARITY_PATH="singularity exec --bind $BIND /software/singularity/containers/Projects/SPSP"

#SEQKIT_EXE="$SINGULARITY_PATH/seqkit-0.12.1-1.ubuntu18.sif seqkit"
SPSP_EXE="$SINGULARITY_PATH/SPSPscripts-2021.01.13-1.ubuntu18.sif"
MAFFT_EXE="$SINGULARITY_PATH/mafft-7.467-1.ubuntu18.sif mafft"
R_EXE="$SINGULARITY_PATH/APE-5.0-1.ubuntu18.sif R --vanilla --quiet --slave --no-save"
IQTREE2_EXE="$SINGULARITY_PATH/IQ_TREE-2.0.6-1.ubuntu18.sif iqtree2"
PARETREE_EXE="$SINGULARITY_PATH/PareTree-1.0.2-1.ubuntu18.sif PareTree"


#MLST_DIR=MLST
VARIANT_DIR=variantcall


usage() { echo "Usage: $0 [-r specimen_analyses directory] [-l laboratory] [-p project_name] {-s list_of_specimenID_space_separated}
       $0 [-r specimen_analyses directory] {-c clade} {-t taxId}" 1>&2; exit 1; }
while getopts ":r:l:p:s:c:t:" option; do
    case "${option}" in
        r)
            r=${OPTARG}
            ;;
        l)
            l=${OPTARG}
            ;;
        p)
            p=${OPTARG}
            ;;
        s)
            s=${OPTARG}
            ;;
        c)
            c=${OPTARG}
            ;;
        t)
            t=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))
if [ -z "${r}" ]; then
    usage
fi
if [ -n "${l}" ]; then
    if [ -z "${p}" ]; then
        usage
    fi
fi
if [ -n "${p}" ]; then
    if [ -z "${l}" ]; then
        usage
    fi
fi



# Default is Viruses
CLADE="virus"
if [ -n "${c}" ]; then
    CLADE=${c}
fi

# Set default species related to clade
if [ $CLADE = 'virus' ]; then
    #NOTE SARScov2 taxid
    SPECIES=2697049
    BIND="$BIND/viruses"
else
    echo "Invalid clade [${c}]" 1>&2
    exit 1
fi


if [ -n "${t}" ]; then
    SPECIES=${t}
fi

PROJECT=$BIND/processed_data/comparative_analyses/project_based
GLOBAL=$BIND/processed_data/comparative_analyses/species_based/$SPECIES/global_tree

REF_GENOME=$BIND/reference_data/$SPECIES/reference_genome/reference.fasta
HOMOPLASY_EXT_FILE=$BIND/reference_data/$SPECIES/phylogeny/maskHomoplasic.txt



# Concatenate consensus sequences
echo "*** Prepare sequences ***"
OUTDIR=$GLOBAL
if [ -n "${l}" ]; then #with ${p} tested before
    #NOTE cgMLST for compatibility reasons with bacteria scripts
    OUTDIR=$PROJECT/${l}/${p}/cgMLST
    mkdir -p $OUTDIR
    rm -f $OUTDIR/ALL_consensus.fasta
    specimenIDs=`ls -d1 ${r}/* | sed -e 's@^.*/@@' | perl -pe 's@\n@ @'`
    if [ -n "${s}" ]; then
        specimenIDs=${s}
    fi

    for dir in ${specimenIDs}
    do
        cat ${r}/${dir}/$VARIANT_DIR/*.consensus.fasta >> $OUTDIR/ALL_consensus.fasta
    done
else
    OUTDIR=$GLOBAL
    mkdir -p $OUTDIR
    mkdir -p $OUTDIR/../ST_trees
    cat ${r}/*/$VARIANT_DIR/*.consensus.fasta > $OUTDIR/ALL_consensus.fasta
fi



# Alignment
# 1) concatenate all sequences and Wuhan reference (NC_045512.2) into a single fasta file: consensus_Wuhan.fasta
# 2) clean reads that contain excessive Ns using biopython script sequence_cleaner.py: (0 for no min lenght; allow for 10% as our internal filter)
#cat $OUTDIR/ALL_consensus.fasta $REF_GENOME > $OUTDIR/consensus_Wuhan.fasta
ln -fs $OUTDIR/ALL_consensus.fasta $OUTDIR/consensus_Wuhan.fasta
# Go there because sequence_cleaner.py writes where it is!
cd $OUTDIR/
# 10% of Ns allowed, output is clear_consensus_Wuhan.fasta
#FIXME sequence_cleaner.py add a prefix to the input file, so cannot work with path containing '/' !!!
#FIXME if success or failure always exit code 0 !!!
$SPSP_EXE python3 /usr/local/bin/sequence_cleaner.py consensus_Wuhan.fasta 0 10  >$OUTDIR/sequence_cleaner.log 2>&1
status_seq_cleaner=$?
if [ $status_seq_cleaner -ne 0 ]; then
    echo "Error with sequence_cleaner.py [$status_seq_cleaner]" >&2
    exit 1
fi
echo "*** Alignment ***"
# no gaps allowed (--keeplength)
#NOTE use the original reference - not cleaned - to map on it
cp $REF_GENOME $OUTDIR/NC_045512_SARSCov2_ref.fasta
sed -i 's@>.*@>NC_045512.2 Severe acute respiratory syndrome coronavirus 2 isolate Wuhan-Hu-1, complete genome@' $OUTDIR/NC_045512_SARSCov2_ref.fasta
$MAFFT_EXE --thread ${SLURM_CPUS_PER_TASK:=8} --reorder --keeplength --nwildcard --mapout --kimura 1 --addfragments clear_consensus_Wuhan.fasta --auto $OUTDIR/NC_045512_SARSCov2_ref.fasta > $OUTDIR/aln_clear_consensus_Wuhan.fasta 2>$OUTDIR/mafft.log
status_mafft=$?
if [ $status_mafft -ne 0 ]; then
    echo "Error with MAFFT [$status_mafft]" >&2
    exit 1
fi



# Trim ends and mask homoplasic sites
#nucleotides need to be all small caps for this step
echo "*** Trimming and homoplastic sites masking ***"
#Write R script
cat << \EOF > script.R
library(msa)
library(seqinr)
library(IRanges)

msa=readDNAMultipleAlignment("__OUTDIR__/aln_clear_consensus_Wuhan.fasta") # read alignment
maskfile=read.table("__HOMOPLASY_EXT_FILE__") # read masking file
colM <- IRanges(start=c(maskfile$V2), end=c(maskfile$V3))
masked_msa <-msa
colmask(masked_msa) <- colM

#save as fasta
DNAStr = as(masked_msa, "DNAStringSet") # removes masked columns
writeXStringSet(DNAStr, file="__OUTDIR__/aln_HomoplasicMasked_Nclear_consensus_Wuhan.fa")
EOF
sed -i "s@__OUTDIR__@$OUTDIR@; s@__HOMOPLASY_EXT_FILE__@$HOMOPLASY_EXT_FILE@" script.R
$R_EXE <script.R >$OUTDIR/R.log  2>&1 | grep -v 'Read'



# Make tree using iqtree2
echo "*** Make tree ***"
#NOTE -o option is for outgroup
#NOTE remove bootstrap (-bb 1000) for tests (less than 3 seq)
$IQTREE2_EXE -s $OUTDIR/aln_HomoplasicMasked_Nclear_consensus_Wuhan.fa -m GTR+F+R3 -nt ${SLURM_CPUS_PER_TASK:=8} -alrt 1000 -o NC_045512.2 -redo > $OUTDIR/iqtree2.log 2>&1
status_iqtree2=$?
rm -f $OUTDIR/aln_HomoplasicMasked_Nclear_consensus_Wuhan.fa.log # Duplicated with iqtree2.log
if [ $status_iqtree2 -ne 0 ]; then
    echo "Error with IQ-TREE2 [$status_iqtree2]" >&2
    exit 1
fi

#before creating symlinks, prune tree from Wuhan reference and bootstrap values
mv $OUTDIR/aln_HomoplasicMasked_Nclear_consensus_Wuhan.fa.treefile $OUTDIR/aln_HomoplasicMasked_Nclear_consensus_Wuhan.fa.treefile.ori
$PARETREE_EXE -t O -f $OUTDIR/aln_HomoplasicMasked_Nclear_consensus_Wuhan.fa.treefile.ori -del NC_045512.2 -nbs >$OUTDIR/PareTree.log 2>&1
status_paretree=$?
if [ $status_paretree -ne 0 ]; then
    echo "Error with PareTree [$status_paretree]" >&2
    exit 1
fi
mv `grep '^Pared tree in file: ' $OUTDIR/PareTree.log | sed -e 's/^Pared tree in file: //'` $OUTDIR/aln_HomoplasicMasked_Nclear_consensus_Wuhan.fa.treefile


#symlinks for compatibility reasons!
ln -fs aln_HomoplasicMasked_Nclear_consensus_Wuhan.fa.treefile  reference_align.final_tree.nwk
ln -fs aln_HomoplasicMasked_Nclear_consensus_Wuhan.fa.treefile  cgMLST_tree.nwk
ln -fs aln_HomoplasicMasked_Nclear_consensus_Wuhan.fa.treefile  ST-0_align.final_tree.nwk
if [ ! -n "${l}" ]; then
    ln -fs $GLOBAL $OUTDIR/../ST_trees/ST-0
fi

echo "*** DONE ***"
exit 0

