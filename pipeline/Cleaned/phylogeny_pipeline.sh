#!/bin/bash

#SBATCH --qos=6hours
#SBATCH --mem=50G
#SBATCH --cpus-per-task=16
#SBATCH --job-name=snp_calling
#SBATCH --array=1-50

#TODO Where are (stored) mentalist db? How to build them?

#FIXME Use arguments as input, do NOT hardcode sample (?) ids!!!
array=(mock run185-atcc-49775 607887-15 800236-15 400871-16 602191-17-B 401130-15 501030-17-B 500190-15 801826-17 702924-15 504117-15 140662-17 611230-17 608367-16 501058-15 504085-17 806392-15 801866-17-B 608016-16 208880-15 604392-16 612085-15 808328-17 401896-17 126616-16 500785-15 801777-15 402240-15 503342-15 807279-17 504202-15 202580-16 807990-16 601251-16 611238-17 803303-15 709802-17 602701-16 803364-17 401188-15 502776-15 502312-17 611177-17 803263-15 806968-17 602220-16 608718-16 607090-17 802643-15 607651-17)

reference=MLST/genomes/ST-genomes/reference_strain.fa

#SEB: Used?
#export _JAVA_OPTIONS="-Xmx10g"

export sample_id=${array["$SLURM_ARRAY_TASK_ID"]}


# Commands
SINGULARITY_PATH="singularity exec /software/singularity/containers/Projects/SPSP"

GATK_EXE="$SINGULARITY_PATH/GATK-3.8.1.0.gf15c1c3ef-1.ubuntu.simg GenomeAnalysisTK"
PICARD_EXE="$SINGULARITY_PATH/Picard-2.18.20-1.ubuntu.simg picard-tools"
TRIMMOMATIC_EXE="$SINGULARITY_PATH/Trimmomatic-0.38-1.ubuntu.simg trimmomatic"
MENTALIST_EXE="$SINGULARITY_PATH/MentaLiST-0.2.4-1.biocontainers.simg mentalist"
SPADES_EXE="$SINGULARITY_PATH/SPAdes-3.13.0-1.ubuntu.simg spades.py"
SAMTOOLS_EXE="$SINGULARITY_PATH/samtools-1.9-1.biocontainers.simg samtools"
BWA_EXE="$SINGULARITY_PATH/BWA-0.7.17-1.ubuntu.simg bwa"
VCFTOOLS_EXE="$SINGULARITY_PATH/VCFtools-0.1.16-1.ubuntu.simg vcftools"
VCFLIB_EXE="$SINGULARITY_PATH/vcflib-1.0.0_rc1-1.biocontainers.simg vcffilter"


# Paths
#TODO ...

raw_R1=../reads/"$sample_id"_R1.fastq.gz
raw_R2=../reads/"$sample_id"_R2.fastq.gz
#R1=../reads/"$sample_id"_R1.fastq.gz
#R2=../reads/"$sample_id"_R2.fastq.gz


#Trimm reads
mkdir -p results/"$sample_id"/0_trimming
$TRIMMOMATIC_EXE PE -threads "$SLURM_CPUS_PER_TASK" -phred33 "$raw_R1" "$raw_R2" results/"$sample_id"/0_trimming/r1.fastq.gz results/"$sample_id"/0_trimming/r1.not-paired.fastq.gz results/"$sample_id"/0_trimming/r2.fastq.gz results/"$sample_id"/0_trimming/r2.not-paired.fastq.gz ILLUMINACLIP:/usr/local/Trimmomatic-0.38/adapters/NexteraPE-PE.fa:2:30:10 SLIDINGWINDOW:4:12 MINLEN:100 2> results/"$sample_id"/0_trimming/quality_read_trimm_info
R1=results/"$sample_id"/0_trimming/r1.fastq.gz
R2=results/"$sample_id"/0_trimming/r2.fastq.gz


#Typing
mkdir -p results/"$sample_id"/typing/

$MENTALIST_EXE call -o results/"$sample_id"/typing/"$sample_id"_mlst.txt --db MLST/mlstdb/staaur_mlst.db -1 "$R1" -2 "$R2"
$MENTALIST_EXE call -o results/"$sample_id"/typing/"$sample_id"_cgmlst.txt  --output_votes --output_special  --db MLST/cgmlstdb/staaur_cgmlst.db -1 "$R1" -2 "$R2"

sequencetype="$(awk 'END {print $9}' results/"$sample_id"/typing/"$sample_id"_mlst.txt)"


#Assembly
mkdir -p results/"$sample_id"/
$SPADES_EXE --cov-cutoff auto --careful --mismatch-correction -t "$SLURM_CPUS_PER_TASK" -k 21,33,55,77,99,127 -1 "$R1" -2 "$R2" -o results/"$sample_id"/spades_assembly


#Copy results
if [ ! -f MLST/genomes/ST-genomes/ST-"$sequencetype"-*.fa ] ; then
    cp results/"$sample_id"/spades_assembly/scaffolds.fasta MLST/genomes/ST-genomes/ST-"$sequencetype"-"$sample_id".fa
fi



#Functions
function mapping {
    #index Reference
    mkdir -p results/"$sample_id"/"$1"/"$2"
    cp "$3" results/"$sample_id"/"$1"/"$2"/reference.fa
    $SAMTOOLS_EXE faidx results/"$sample_id"/"$1"/"$2"/reference.fa
    $PICARD_EXE CreateSequenceDictionary R=results/"$sample_id"/"$1"/"$2"/reference.fa O=results/"$sample_id"/"$1"/"$2"/reference.dict
    $BWA_EXE index results/"$sample_id"/"$1"/"$2"/reference.fa

    #map reads and format bam
    $BWA_EXE mem -t "$SLURM_CPUS_PER_TASK" results/"$sample_id"/"$1"/"$2"/reference.fa "$R1" "$R2" > results/"$sample_id"/"$1"/"$2"/"$sample_id".sam
    $SAMTOOLS_EXE sort -@ "$SLURM_CPUS_PER_TASK" -T results/"$sample_id"/"$1"/"$2"/_temp -o results/"$sample_id"/"$1"/"$2"/"$sample_id"_sorted.bam results/"$sample_id"/"$1"/"$2"/"$sample_id".sam
    $SAMTOOLS_EXE rmdup -S results/"$sample_id"/"$1"/"$2"/"$sample_id"_sorted.bam results/"$sample_id"/"$1"/"$2"/"$sample_id"_sorted.rmdup.sorted.bam
    $PICARD_EXE AddOrReplaceReadGroups I=results/"$sample_id"/"$1"/"$2"/"$sample_id"_sorted.rmdup.sorted.bam O=results/"$sample_id"/"$1"/"$2"/"$sample_id"_sorted.rmdup.grouped.bam RGID="$sample_id" RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=20
    $SAMTOOLS_EXE index results/"$sample_id"/"$1"/"$2"/"$sample_id"_sorted.rmdup.grouped.bam

    #FIXME Why is $folder_name not defined, and not used?
    "$folder_name"
}


function call_variants {
    #call variants
    mapping "$1" 1_SNPs MLST/genomes/ST-genomes/"$1"-*.fa
    $GATK_EXE -T HaplotypeCaller -ploidy 1 -R results/"$sample_id"/"$1"/1_SNPs/reference.fa -I results/"$sample_id"/"$1"/1_SNPs/"$sample_id"_sorted.rmdup.grouped.bam  -o results/"$sample_id"/"$1"/1_SNPs/"$sample_id"_sorted.rmdup.grouped.vcf.gz
    $VCFTOOLS_EXE --gzvcf results/"$sample_id"/"$1"/1_SNPs/"$sample_id"_sorted.rmdup.grouped.vcf.gz --remove-indels --recode --recode-INFO-all --stdout | $VCFLIB_EXE -f " DP  > 9 & AF > 0.9 " > results/"$sample_id"/"$1"/1_SNPs/"$sample_id"_sorted.rmdup.grouped.filtered.snps.vcf
    $GATK_EXE -T FastaAlternateReferenceMaker -R results/"$sample_id"/"$1"/1_SNPs/reference.fa -o results/"$sample_id"/"$1"/1_SNPs/reference_snps.fa -V results/"$sample_id"/"$1"/1_SNPs/"$sample_id"_sorted.rmdup.grouped.filtered.snps.vcf

    #mask bases
    mapping "$1" 2_readdepth results/"$sample_id"/"$1"/1_SNPs/reference_snps.fa
    $GATK_EXE -T HaplotypeCaller -ploidy 1 -R results/"$sample_id"/"$1"/2_readdepth/reference.fa -I results/"$sample_id"/"$1"/2_readdepth/"$sample_id"_sorted.rmdup.grouped.bam  -o results/"$sample_id"/"$1"/2_readdepth/"$sample_id"_sorted.rmdup.grouped.g.vcf.gz -ERC BP_RESOLUTION
    zcat results/"$sample_id"/"$1"/2_readdepth/"$sample_id"_sorted.rmdup.grouped.g.vcf.gz | $VCFLIB_EXE -g " DP  < 10 " | sed 's/<NON_REF>/N/' | awk '{if(substr($1, 1, 1)=="#") print ; else if ($4!="N" && $10!=".") print $1"\t"$2"\t"$3"\t"substr($4, 1, 1)"\tN\t"$6"\t"$7"\t"$8"\t"$9"\t"$10}' > results/"$sample_id"/"$1"/2_readdepth/"$sample_id"_sorted.rmdup.grouped.lowcov.vcf
    $GATK_EXE -T FastaAlternateReferenceMaker -R results/"$sample_id"/"$1"/2_readdepth/reference.fa -o results/"$sample_id"/"$1"/2_readdepth/reference_snps_lowcov.fa -V results/"$sample_id"/"$1"/2_readdepth/"$sample_id"_sorted.rmdup.grouped.lowcov.vcf

    python3 MLST/genomes/get_core_genes.py results/"$sample_id"/"$1"/2_readdepth/reference_snps_lowcov.fa "$sample_id" "$1" > results/"$sample_id"/"$1"/"$sample_id"_on_"$1".fasta
}

call_variants reference
call_variants ST-"$sequencetype"

exit 0

