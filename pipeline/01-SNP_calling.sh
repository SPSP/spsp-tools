#!/bin/bash

#SBATCH --export=NONE
#SBATCH --mem=50G
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --job-name=snp_calling


export LC_ALL="C"
BIND=/data

# Commands
#singularity exec --bind <data_path if not run from /tmp or /home> /software/singularity/containers/Projects/SPSP
SINGULARITY_PATH="singularity exec --bind $BIND /software/singularity/containers/Projects/SPSP"

MENTALIST_EXE="$SINGULARITY_PATH/MentaLiST-0.2.4-1.ubuntu.simg mentalist"
SPADES_EXE="$SINGULARITY_PATH/SPAdes-3.13.0-1.ubuntu.simg spades.py"
SAMTOOLS_EXE="$SINGULARITY_PATH/samtools-1.9-1.biocontainers.simg samtools"
PICARD_EXE="$SINGULARITY_PATH/Picard-2.18.20-1.ubuntu.simg picard-tools"
BWA_EXE="$SINGULARITY_PATH/BWA-0.7.17-1.ubuntu.simg bwa"
GATK_EXE="$SINGULARITY_PATH/GATK-3.8.1.0.gf15c1c3ef-1.ubuntu.simg GenomeAnalysisTK"
VCFTOOLS_EXE="$SINGULARITY_PATH/VCFtools-0.1.16-1.ubuntu.simg vcftools"
VCFLIB_EXE="$SINGULARITY_PATH/vcflib-1.0.0_rc1-1.biocontainers.simg vcffilter"
SPSP_EXE="$SINGULARITY_PATH/SPSPscripts-2021.01.13-1.ubuntu18.sif"


MLST_DIR=MLST
ASSEMBLY_DIR=assembly

#TODO Where are (stored) mentalist db? How to build them?


usage() { echo -e "Usage: $0 [-f trimmed_forward_read_file] [-r trimmed_reverse_read_file] {-s sampleId_to_use} {-c bacteria} {-t taxId} {-o output_folder}\n\tor\n       $0 [-f trimmed_unpaired_read_file]                               {-s sampleId_to_use} {-c bacteria} {-t taxId} {-o output_folder}" 1>&2; exit 1; }
while getopts ":f:r:s:c:t:o:" option; do
    case "${option}" in
        f)
            f=${OPTARG}
            ;;
        r)
            r=${OPTARG}
            ;;
        s)
            s=${OPTARG}
            ;;
        c)
            c=${OPTARG}
            ;;
        t)
            t=${OPTARG}
            ;;
        o)
            o=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))
PAIRED_END=true
if [ -z "${f}" ]; then
    usage
fi
if [ -z "${r}" ]; then
    PAIRED_END=false
fi


# Default is Bacteria
CLADE="bacteria"
if [ -n "${c}" ]; then
    CLADE=${c}
fi


# Set default species related to clade
if [ $CLADE = 'bacteria' ]; then
    #NOTE S. aureus taxid
    SPECIES=1280
    BIND="$BIND/bacteria"
else
    echo "Invalid clade [${c}]" 1>&2
    exit 1
fi


if [ -n "${t}" ]; then
    SPECIES=${t}
fi
REF_GENOME=$BIND/reference_data/MLST/$SPECIES/genomes/ST-genomes
MLSTDB=$BIND/reference_data/MLST/$SPECIES/mlstdb
CGMLSTDB=$BIND/reference_data/MLST/$SPECIES/cgmlstdb


OUTPUT="."
if [ -n "${o}" ]; then
    OUTPUT=${o}
fi


# Common prefix between forward and reverse read files
if [ "$PAIRED_END" = true ]; then
    PREFIX=`printf "%s\n%s\n" "${f}" "${r}" | sed -e 'N;s/^\(.*\).*\n\1.*$/\1/' | xargs basename | sed -e 's|[-_][Rr]$||'` # Remove trailing delimiter for forward/reverse paired-end reads
else
    PREFIX=`printf "%s\n"     "${f}"        | sed -e 'N;s/^\(.*\).*\n\1.*$/\1/' | xargs basename | sed -e 's|[-_][Rr]$||'` # Remove trailing delimiter for forward/reverse paired-end reads
fi
#Force prefix from command line
if [ -n "${s}" ]; then
    PREFIX=${s}
fi
OUTDIR=$OUTPUT/$PREFIX

# Create result folder
mkdir -p $OUTDIR


# Typing
echo "*** Typing ***"
mkdir -p $OUTDIR/$MLST_DIR
if [ "$PAIRED_END" = true ]; then
    $MENTALIST_EXE call -o $OUTDIR/$MLST_DIR/${PREFIX}_mlst.txt                                     --db $MLSTDB/staaur_mlst.db     -1 "${f}" -2 "${r}"  >$OUTDIR/$MLST_DIR/${PREFIX}_mlst.log   2>&1
    $MENTALIST_EXE call -o $OUTDIR/$MLST_DIR/${PREFIX}_cgmlst.txt  --output_votes --output_special  --db $CGMLSTDB/staaur_cgmlst.db -1 "${f}" -2 "${r}"  >$OUTDIR/$MLST_DIR/${PREFIX}_cgmlst.log 2>&1
else
    $MENTALIST_EXE call -o $OUTDIR/$MLST_DIR/${PREFIX}_mlst.txt                                     --db $MLSTDB/staaur_mlst.db     -1 "${f}"            >$OUTDIR/$MLST_DIR/${PREFIX}_mlst.log   2>&1
    $MENTALIST_EXE call -o $OUTDIR/$MLST_DIR/${PREFIX}_cgmlst.txt  --output_votes --output_special  --db $CGMLSTDB/staaur_cgmlst.db -1 "${f}"            >$OUTDIR/$MLST_DIR/${PREFIX}_cgmlst.log 2>&1
fi

#Use proper sample_id filename in ALL files
sed -i -e 's/^\('${PREFIX}'\)[-_][Rr]/\1/g' $OUTDIR/$MLST_DIR/${PREFIX}_mlst.txt*
sed -i -e 's/^\('${PREFIX}'\)[-_][Rr]/\1/g' $OUTDIR/$MLST_DIR/${PREFIX}_cgmlst.txt*

sequencetype="$(               awk 'END {print $9}'                                                        $OUTDIR/$MLST_DIR/${PREFIX}_mlst.txt)"
sequencetype_detailed="$(      awk 'END {printf "%s__%s__%s__%s__%s__%s__%s", $2, $3, $4, $5, $6, $7, $8}' $OUTDIR/$MLST_DIR/${PREFIX}_mlst.txt)"
sequencetype_clonal_complex="$(awk 'END {print $10}'                                                       $OUTDIR/$MLST_DIR/${PREFIX}_mlst.txt)"
# Create MLST report file
echo "{\"MLST\":\"$sequencetype\", \"MLST_detailed\":\"$sequencetype_detailed\",\"MLST_clonal_complex\":\"$sequencetype_clonal_complex\"}" >$OUTDIR/$MLST_DIR/${PREFIX}_mlst.json


# Assembly
echo "*** Assembly ***"
mkdir -p $OUTDIR/$ASSEMBLY_DIR
#NOTE Spades re-does some read trimming!
if [ "$PAIRED_END" = true ]; then
    $SPADES_EXE --cov-cutoff auto --careful --mismatch-correction -t ${SLURM_CPUS_PER_TASK:=8} -k 21,33,55,77,99,127 -1 "${f}" -2 "${r}" -o $OUTDIR/$ASSEMBLY_DIR >$OUTDIR/$ASSEMBLY_DIR/${PREFIX}_spades.log 2>&1
else
    $SPADES_EXE --cov-cutoff auto --careful --mismatch-correction -t ${SLURM_CPUS_PER_TASK:=8} -k 21,33,55,77,99,127 -s "${f}"           -o $OUTDIR/$ASSEMBLY_DIR >$OUTDIR/$ASSEMBLY_DIR/${PREFIX}_spades.log 2>&1
fi
status_spades=$?
if [ $status_spades -ne 0 ]; then
    echo "Error with SPADES [$status_spades]. Invalid FASTQ format or path?" >&2
    exit 1
fi
#CLEANING: Remove SPAdes temporary folder (empty)
rmdir $OUTDIR/$ASSEMBLY_DIR/tmp/
mv $OUTDIR/$ASSEMBLY_DIR/${PREFIX}_spades.log $OUTDIR/


# Copy results
if [ ! -f $REF_GENOME/ST-"$sequencetype"-*.fa ] ; then
    cp $OUTDIR/$ASSEMBLY_DIR/scaffolds.fasta $REF_GENOME/ST-"$sequencetype"-"$PREFIX".fa
fi
#CLEANING: Compress SPAdes assembly folder
#FIXME compress until ST-0 (incomplete ST) cases are fixed, then rm ?
ASSEMBLY_ARCHIVE='assembly.tar'
pushd $OUTDIR/$ASSEMBLY_DIR/ >/dev/null
tar cfSp ../$ASSEMBLY_ARCHIVE * && nice xz -9 ../$ASSEMBLY_ARCHIVE && rm -rf * && mv ../${ASSEMBLY_ARCHIVE}.xz ../${PREFIX}_spades.log .
popd >/dev/null


# Functions
function mapping {
    #index reference
    mkdir -p $OUTDIR/$MLST_DIR/"$1"/"$2"
    cp "$3" $OUTDIR/$MLST_DIR/"$1"/"$2"/reference.fa
    $SAMTOOLS_EXE faidx $OUTDIR/$MLST_DIR/"$1"/"$2"/reference.fa >$OUTDIR/$MLST_DIR/"$1"/"$2"/reference_samtools-faidx.log 2>&1
    $PICARD_EXE CreateSequenceDictionary R=$OUTDIR/$MLST_DIR/"$1"/"$2"/reference.fa O=$OUTDIR/$MLST_DIR/"$1"/"$2"/reference.dict >$OUTDIR/$MLST_DIR/"$1"/"$2"/reference_picard.log 2>&1
    $BWA_EXE index $OUTDIR/$MLST_DIR/"$1"/"$2"/reference.fa >$OUTDIR/$MLST_DIR/"$1"/"$2"/reference_bwa-index.log 2>&1

    #map reads and format bam
    if [ "$PAIRED_END" = true ]; then
        $BWA_EXE mem -t ${SLURM_CPUS_PER_TASK:=8} $OUTDIR/$MLST_DIR/"$1"/"$2"/reference.fa "${f}" "${r}" > $OUTDIR/$MLST_DIR/"$1"/"$2"/${PREFIX}.sam 2> $OUTDIR/$MLST_DIR/"$1"/"$2"/reference_bwa-mem.log
    else
        $BWA_EXE mem -t ${SLURM_CPUS_PER_TASK:=8} $OUTDIR/$MLST_DIR/"$1"/"$2"/reference.fa "${f}"        > $OUTDIR/$MLST_DIR/"$1"/"$2"/${PREFIX}.sam 2> $OUTDIR/$MLST_DIR/"$1"/"$2"/reference_bwa-mem.log
    fi
    $SAMTOOLS_EXE sort -@ ${SLURM_CPUS_PER_TASK:=8} -T $OUTDIR/$MLST_DIR/"$1"/"$2"/_temp -o $OUTDIR/$MLST_DIR/"$1"/"$2"/${PREFIX}_sorted.bam $OUTDIR/$MLST_DIR/"$1"/"$2"/${PREFIX}.sam >$OUTDIR/$MLST_DIR/"$1"/"$2"/reference_samtools-sort.log 2>&1
    $SAMTOOLS_EXE rmdup -S $OUTDIR/$MLST_DIR/"$1"/"$2"/${PREFIX}_sorted.bam $OUTDIR/$MLST_DIR/"$1"/"$2"/${PREFIX}_sorted.rmdup.sorted.bam >$OUTDIR/$MLST_DIR/"$1"/"$2"/reference_samtools-rmdup.log 2>&1
    $PICARD_EXE AddOrReplaceReadGroups I=$OUTDIR/$MLST_DIR/"$1"/"$2"/${PREFIX}_sorted.rmdup.sorted.bam O=$OUTDIR/$MLST_DIR/"$1"/"$2"/${PREFIX}_sorted.rmdup.grouped.bam RGID=${PREFIX} RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=20 >$OUTDIR/$MLST_DIR/"$1"/"$2"/${PREFIX}_sorted.rmdup.grouped_picard.log 2>&1
    $SAMTOOLS_EXE index $OUTDIR/$MLST_DIR/"$1"/"$2"/${PREFIX}_sorted.rmdup.grouped.bam >$OUTDIR/$MLST_DIR/"$1"/"$2"/reference_samtools-index.log 2>&1
}


function call_variants {
    #call variants
    mapping "$1" 1_SNPs $REF_GENOME/"$1"-*.fa
    #NOTE https://gatkforums.broadinstitute.org/gatk/discussion/1975/how-can-i-use-parallelism-to-make-gatk-tools-run-faster
    # while HaplotypeCaller supports -nct (controls the number of CPU threads allocated to each data thread) in principle, many have reported that it is not very stable (random crashes may occur -- but if there is no crash, results will be correct). We prefer not to use this option with HC; use it at your own risk.
    $GATK_EXE -T HaplotypeCaller -ploidy 1 -nt 1 -nct ${SLURM_CPUS_PER_TASK:=8} -R $OUTDIR/$MLST_DIR/"$1"/1_SNPs/reference.fa      -I $OUTDIR/$MLST_DIR/"$1"/1_SNPs/${PREFIX}_sorted.rmdup.grouped.bam      -o $OUTDIR/$MLST_DIR/"$1"/1_SNPs/${PREFIX}_sorted.rmdup.grouped.vcf.gz >$OUTDIR/$MLST_DIR/"$1"/1_SNPs/${PREFIX}_sorted.rmdup.grouped.vcf_gatk.log 2>&1
    #Possible issue when several vcftools commands at the same time with always the same stderr/log filename! So change directory
    pushd $OUTDIR/$MLST_DIR/"$1"/1_SNPs/ >/dev/null
    $VCFTOOLS_EXE --gzvcf $OUTDIR/$MLST_DIR/"$1"/1_SNPs/${PREFIX}_sorted.rmdup.grouped.vcf.gz --remove-indels --recode --recode-INFO-all --stdout 2>out.log | $VCFLIB_EXE -f " DP  > 9 & AF > 0.9 " > $OUTDIR/$MLST_DIR/"$1"/1_SNPs/${PREFIX}_sorted.rmdup.grouped.filtered.snps.vcf; mv out.log $OUTDIR/$MLST_DIR/"$1"/1_SNPs/${PREFIX}_sorted.rmdup.grouped_vcftools.log
    popd >/dev/null
    $GATK_EXE -T FastaAlternateReferenceMaker -R $OUTDIR/$MLST_DIR/"$1"/1_SNPs/reference.fa      -o $OUTDIR/$MLST_DIR/"$1"/1_SNPs/reference_snps.fa             -V $OUTDIR/$MLST_DIR/"$1"/1_SNPs/${PREFIX}_sorted.rmdup.grouped.filtered.snps.vcf > $OUTDIR/$MLST_DIR/"$1"/1_SNPs/reference_snps_gatk.log 2>&1

    #mask bases
    mapping "$1" 2_readdepth $OUTDIR/$MLST_DIR/"$1"/1_SNPs/reference_snps.fa
    $GATK_EXE -T HaplotypeCaller -ploidy 1 -nt 1 -nct ${SLURM_CPUS_PER_TASK:=8} -R $OUTDIR/$MLST_DIR/"$1"/2_readdepth/reference.fa -I $OUTDIR/$MLST_DIR/"$1"/2_readdepth/${PREFIX}_sorted.rmdup.grouped.bam -o $OUTDIR/$MLST_DIR/"$1"/2_readdepth/${PREFIX}_sorted.rmdup.grouped.g.vcf.gz -ERC BP_RESOLUTION > $OUTDIR/$MLST_DIR/"$1"/2_readdepth/${PREFIX}_sorted.rmdup.grouped.g.vcf_gatk.log 2>&1
    zcat $OUTDIR/$MLST_DIR/"$1"/2_readdepth/${PREFIX}_sorted.rmdup.grouped.g.vcf.gz | $VCFLIB_EXE -g " DP  < 10 " | sed 's/<NON_REF>/N/' | awk '{if(substr($1, 1, 1)=="#") print ; else if ($4!="N" && $10!=".") print $1"\t"$2"\t"$3"\t"substr($4, 1, 1)"\tN\t"$6"\t"$7"\t"$8"\t"$9"\t"$10}' > $OUTDIR/$MLST_DIR/"$1"/2_readdepth/${PREFIX}_sorted.rmdup.grouped.lowcov.vcf
    $GATK_EXE -T FastaAlternateReferenceMaker -R $OUTDIR/$MLST_DIR/"$1"/2_readdepth/reference.fa -o $OUTDIR/$MLST_DIR/"$1"/2_readdepth/reference_snps_lowcov.fa -V $OUTDIR/$MLST_DIR/"$1"/2_readdepth/${PREFIX}_sorted.rmdup.grouped.lowcov.vcf > $OUTDIR/$MLST_DIR/"$1"/2_readdepth/reference_snps_lowcov_gatk.log 2>&1

    $SPSP_EXE python3 /usr/local/bin/get_core_genes.py $OUTDIR/$MLST_DIR/"$1"/2_readdepth/reference_snps_lowcov.fa ${PREFIX} "$1" > $OUTDIR/$MLST_DIR/"$1"/${PREFIX}_on_"$1".fasta
}

echo "*** Call variants ${PREFIX} - reference ***"
call_variants reference
echo "*** Call variants ${PREFIX} - ST-$sequencetype ***"
call_variants ST-"$sequencetype"

#CLEANING: Remove MLST reference and sequence-type sub-folders because not used anymore after analysis
pushd $OUTDIR/$MLST_DIR/reference/ >/dev/null
rm -rf 1_SNPs/ 2_readdepth/
popd >/dev/null
pushd $OUTDIR/$MLST_DIR/ST-"$sequencetype" >/dev/null
rm -rf 1_SNPs/ 2_readdepth/
popd >/dev/null

#CLEANING: Compress largest cgMLST txt files
pushd $OUTDIR/$MLST_DIR/ >/dev/null
find . -type f -name \*_cgmlst.txt.\* -size +1k -exec xz -9 {} \;
popd >/dev/null

echo "*** DONE ***"
exit 0

