#!/bin/bash

#SBATCH --export=NONE
#SBATCH --mem=50G
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --job-name=quality_control
#TODO add stderr/stdout filepaths on sbatch command line to control location!
#TODO add account and partition options!

#TODO specimen_id must NOT contain ':' for phylogeny nor '_' for prefix parsing!
#TODO specimenID finishing by "-B" are not valid!


export LC_ALL="C"
BIND=/data

# Commands
#singularity exec --bind <data_path if not run from /tmp or /home> /software/singularity/containers/Projects/SPSP
SINGULARITY_PATH="singularity exec --bind $BIND /software/singularity/containers/Projects/SPSP"

TRIMMOMATIC_EXE="$SINGULARITY_PATH/Trimmomatic-0.38-1.ubuntu.simg trimmomatic"
FASTQC_EXE="$SINGULARITY_PATH/FastQC-0.11.8-1.ubuntu.simg fastqc"
KRAKEN2_EXE="$SINGULARITY_PATH/Kraken2-2.0.9b-1.ubuntu18.sif kraken2"
SPSP_EXE="$SINGULARITY_PATH/SPSPscripts-2021.01.13-1.ubuntu18.sif"
R_EXE="$SINGULARITY_PATH/APE-5.0-1.ubuntu18.sif Rscript"

TRIMMED_DIR=trimmed_FASTQ
FASTQC_DIR=FastQC
CONTAMINATION_DIR=contamination
VARIANT_DIR=variantcall


usage() { echo -e "Usage: $0 [-f forward_read_file] [-r reverse_read_file] {-s sampleId_to_use} {-l trimmomatic_adapter} {-c clade} {-t taxId} {-k species name for Kraken} {-o output_folder}\n\tor\n       $0 [-f unpaired_read_file]                       {-s sampleId_to_use} {-l trimmomatic_adapter} {-c clade} {-t taxId} {-k species name for Kraken} {-o output_folder}\n\tor\n       $0 [-m metagenome_fasta_file]                    {-s sampleId_to_use}                          {-c clade} {-t taxId}                              {-o output_folder}" 1>&2; exit 1; }
while getopts ":f:r:m:s:l:c:t:k:o:" option; do
    case "${option}" in
        f)
            f=${OPTARG}
            ;;
        r)
            r=${OPTARG}
            ;;
        m)
            m=${OPTARG}
            ;;
        s)
            s=${OPTARG}
            ;;
        l)
            l=${OPTARG}
            ;;
        c)
            c=${OPTARG}
            ;;
        t)
            t=${OPTARG}
            ;;
        k)
            k=${OPTARG}
            ;;
        o)
            o=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))
METAGENOMICS=false
PAIRED_END=true
if [ -z "${f}" ] && [ -z "${m}" ]; then
    usage
fi
#Force -s option
if [ -z "${s}" ]; then
    usage
fi
INPUT="${f}"
if [ -z "${r}" ]; then
    PAIRED_END=false
fi
if [ -n "${m}" ]; then
    METAGENOMICS=true
    INPUT="${m}"
fi


# Default is Virus
CLADE="virus"
if [ -n "${c}" ]; then
    CLADE=${c}
fi


# Set default species related to clade
if [ $CLADE = 'virus' ]; then
    SPECIES=2697049
#NOTE Use species name in Kraken2 *S*, not strain *S1*
#    SPECIES_NAME="Severe_acute_respiratory_syndrome_coronavirus_2"
    SPECIES_NAME="Severe_acute_respiratory_syndrome-related_coronavirus"
    ADAPTER="adapters.fa"
    BIND="$BIND/viruses"
else
    echo "Invalid clade [${c}]" 1>&2
    exit 1
fi


if [ -n "${t}" ]; then
    SPECIES=${t}
fi

KRAKEN2_DB=$BIND/reference_data/external_db/minikraken2
REF_GENOME=$BIND/reference_data/MLST/$SPECIES/genomes/ST-genomes
if [ $CLADE = 'virus' ]; then
    REF_GENOME=$BIND/reference_data/$SPECIES/reference_genome
fi
TRIMMO_EXT_FILES=$BIND/reference_data/$SPECIES/trimmomatic

if [ -n "${k}" ]; then
    SPECIES_NAME=${k// /_}
    #NOTE cannot pass variable with space(s) to a script within a container, so replace it by '_' to replace it back to ' ' in the script itself
fi


#Assume forward and reverse read files are at the same place
OUTPUT=`dirname $INPUT`
if [ -n "${o}" ]; then
    OUTPUT=${o}
fi


# Adapters
#NOTE Use this simple option currently, with NexteraPE-PE.fa as default
if [ -n "${l}" ]; then
    if [ "${l}" = 'Nextera' ]; then
        ADAPTER="NexteraPE-PE.fa"
    elif [[ "${l}" = 'QIASeq' && "$PAIRED_END" = true ]]; then
        ADAPTER="TruSeq3-PE-2.fa"
    elif [[ "${l}" = 'TruSeq' && "$PAIRED_END" = true ]]; then
        ADAPTER="TruSeq3-PE-2.fa"
    elif [[ "${l}" = 'QIASeq' && "$PAIRED_END" = false ]]; then
        ADAPTER="TruSeq3-SE.fa"
    elif [[ "${l}" = 'TruSeq' && "$PAIRED_END" = false ]]; then
        ADAPTER="TruSeq3-SE.fa"
    else
        echo "Invalid adapter [${l}]" 1>&2
        exit 1
    fi
fi


# Common prefix between forward and reverse read files
#NOTE Assume no '_' in **specimen_ID**
if [ "$PAIRED_END" = true ] && [ "$METAGENOMICS" = false ]; then
    PREFIX=`printf "%s\n%s\n" "${f}" "${r}" | sed -e 'N;s/^\(.*\).*\n\1.*$/\1/' | xargs basename | sed -e 's|_.*$||'` # Remove trailing delimiter for forward/reverse paired-end reads + lab id
elif [ "$PAIRED_END" = false ] && [ "$METAGENOMICS" = false ]; then
    PREFIX=`printf "%s\n"     "${f}"        | sed -e 'N;s/^\(.*\).*\n\1.*$/\1/' | xargs basename | sed -e 's|_.*$||'` # Remove trailing delimiter for forward/reverse paired-end reads + lab id
elif [ "$METAGENOMICS" = true ]; then
    PREFIX=`printf "%s\n"     "${m}"        | sed -e 'N;s/^\(.*\).*\n\1.*$/\1/' | xargs basename | sed -e 's|_.*$||'` # Remove trailing delimiter for forward/reverse paired-end reads + lab id
fi
#Force prefix from command line
if [ -n "${s}" ]; then
    PREFIX=${s}
fi
OUTDIR="$OUTPUT/$PREFIX"

# Create result folder
mkdir -p $OUTDIR


# Shortcut for metagenomic fasta input data
if [ "$METAGENOMICS" = true ]; then
    mkdir -p $OUTDIR/$VARIANT_DIR
    cp ${m} $OUTDIR/$VARIANT_DIR/${PREFIX}.consensus.fasta

    # Generate a fake report file, "quality_status" is always *green* for metagenomic fasta input data
    echo -n '{"FastQC_report_files":[],"QC_stat":{"after_trimming_max":"NA","after_trimming_max_forward":"NA","after_trimming_max_reverse":"NA","after_trimming_mean":"NA","after_trimming_mean_forward":"NA","after_trimming_mean_reverse":"NA","after_trimming_median":"NA","after_trimming_median_forward":"NA","after_trimming_median_reverse":"NA","after_trimming_min":"NA","after_trimming_min_forward":"NA","after_trimming_min_reverse":"NA","before_trimming_max":"NA","before_trimming_max_forward":"NA","before_trimming_max_reverse":"NA","before_trimming_mean":"NA","before_trimming_mean_forward":"NA","before_trimming_mean_reverse":"NA","before_trimming_median":"NA","before_trimming_median_forward":"NA","before_trimming_median_reverse":"NA","before_trimming_min":"NA","before_trimming_min_forward":"NA","before_trimming_min_reverse":"NA"},"depth_coverage":"NA","percent_contamination":"NA","percent_read_length_trimming":"NA","quality_status":"green"}' | sed 's@NA@0@g'> $OUTDIR/report.json

    echo "*** DONE ***"
    exit 0
fi


#NOTE ALL specimen_lab_ID must be hidden to users! So do not use files containing specimen_lab_ID in their names to avoid propagation
mkdir -p $OUTDIR/TMP
if [ "$PAIRED_END" = true ]; then
    cp ${f} $OUTDIR/TMP/$PREFIX-r1.fastq.gz
    cp ${r} $OUTDIR/TMP/$PREFIX-r2.fastq.gz
else
    cp ${f} $OUTDIR/TMP/$PREFIX-r1.fastq.gz
fi


# Trimm reads
echo "*** Trimming ***"
mkdir -p $OUTDIR/$TRIMMED_DIR
if [ "$PAIRED_END" = true ]; then
    if [ $CLADE = 'virus' ]; then
        #first trim
        $TRIMMOMATIC_EXE PE -threads ${SLURM_CPUS_PER_TASK:=8} -phred33 \
            $OUTDIR/TMP/$PREFIX-r1.fastq.gz              $OUTDIR/TMP/$PREFIX-r2.fastq.gz \
            $OUTDIR/TMP/$PREFIX-r1.firsttrimmed.fastq.gz $OUTDIR/TMP/$PREFIX-r1.firsttrimmed.not-paired.fastq.gz \
            $OUTDIR/TMP/$PREFIX-r2.firsttrimmed.fastq.gz $OUTDIR/TMP/$PREFIX-r2.firsttrimmed.not-paired.fastq.gz \
            SLIDINGWINDOW:4:20 MINLEN:125  2> $OUTDIR/$TRIMMED_DIR/read_trimm_info
        #adaptor trim
        $TRIMMOMATIC_EXE PE -threads ${SLURM_CPUS_PER_TASK:=8} -phred33 \
            $OUTDIR/TMP/$PREFIX-r1.firsttrimmed.fastq.gz $OUTDIR/TMP/$PREFIX-r2.firsttrimmed.fastq.gz \
            $OUTDIR/TMP/$PREFIX-r1.no_adaptors.fastq.gz  $OUTDIR/TMP/$PREFIX-r1.no_adaptors.not-paired.fastq.gz \
            $OUTDIR/TMP/$PREFIX-r2.no_adaptors.fastq.gz  $OUTDIR/TMP/$PREFIX-r2.no_adaptors.not-paired.fastq.gz \
            ILLUMINACLIP:$TRIMMO_EXT_FILES/$ADAPTER:2:30:10  2> $OUTDIR/$TRIMMED_DIR/adaptor_read_trimm_info
        #primer trim
        $TRIMMOMATIC_EXE PE -threads ${SLURM_CPUS_PER_TASK:=8} -phred33 \
            $OUTDIR/TMP/$PREFIX-r1.no_adaptors.fastq.gz            $OUTDIR/TMP/$PREFIX-r2.no_adaptors.fastq.gz \
            $OUTDIR/TMP/$PREFIX-r1.no_adaptors_no_primers.fastq.gz $OUTDIR/TMP/$PREFIX-r1.no_adaptors_no_primers.not-paired.fastq.gz \
            $OUTDIR/TMP/$PREFIX-r2.no_adaptors_no_primers.fastq.gz $OUTDIR/TMP/$PREFIX-r2.no_adaptors_no_primers.not-paired.fastq.gz \
            ILLUMINACLIP:$TRIMMO_EXT_FILES/primers.fa:2:30:10  2> $OUTDIR/$TRIMMED_DIR/primer_read_trimm_info
        #final length trim to eliminate shortened reads
        $TRIMMOMATIC_EXE PE -threads ${SLURM_CPUS_PER_TASK:=8} -phred33 \
            $OUTDIR/TMP/$PREFIX-r1.no_adaptors_no_primers.fastq.gz $OUTDIR/TMP/$PREFIX-r2.no_adaptors_no_primers.fastq.gz \
            $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.fastq.gz $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.not-paired.fastq.gz \
            $OUTDIR/$TRIMMED_DIR/$PREFIX-r2.fastq.gz $OUTDIR/$TRIMMED_DIR/$PREFIX-r2.not-paired.fastq.gz \
            SLIDINGWINDOW:4:20 MINLEN:125  2> $OUTDIR/$TRIMMED_DIR/quality_read_trimm_info
    fi
else
    if [ $CLADE = 'virus' ]; then
        #first trim
        $TRIMMOMATIC_EXE SE -threads ${SLURM_CPUS_PER_TASK:=8} -phred33 \
            $OUTDIR/TMP/$PREFIX-r1.fastq.gz \
            $OUTDIR/TMP/$PREFIX-r1.firsttrimmed.fastq.gz $OUTDIR/TMP/$PREFIX-r1.firsttrimmed.not-paired.fastq.gz \
            SLIDINGWINDOW:4:20 MINLEN:125  2> $OUTDIR/$TRIMMED_DIR/read_trimm_info
        #adaptor trim
        $TRIMMOMATIC_EXE SE -threads ${SLURM_CPUS_PER_TASK:=8} -phred33 \
            $OUTDIR/TMP/$PREFIX-r1.firsttrimmed.fastq.gz \
            $OUTDIR/TMP/$PREFIX-r1.no_adaptors.fastq.gz  $OUTDIR/TMP/$PREFIX-r1.no_adaptors.not-paired.fastq.gz \
            ILLUMINACLIP:$TRIMMO_EXT_FILES/$ADAPTER:2:30:10  2> $OUTDIR/$TRIMMED_DIR/adaptor_read_trimm_info
        #primer trim
        $TRIMMOMATIC_EXE SE -threads ${SLURM_CPUS_PER_TASK:=8} -phred33 \
            $OUTDIR/TMP/$PREFIX-r1.no_adaptors.fastq.gz \
            $OUTDIR/TMP/$PREFIX-r1.no_adaptors_no_primers.fastq.gz $OUTDIR/TMP/$PREFIX-r1.no_adaptors_no_primers.not-paired.fastq.gz \
            ILLUMINACLIP:$TRIMMO_EXT_FILES/primers.fa:2:30:10  2> $OUTDIR/$TRIMMED_DIR/primer_read_trimm_info
        #final length trim to eliminate shortened reads
        $TRIMMOMATIC_EXE SE -threads ${SLURM_CPUS_PER_TASK:=8} -phred33 \
            $OUTDIR/TMP/$PREFIX-r1.no_adaptors_no_primers.fastq.gz \
            $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.fastq.gz $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.not-paired.fastq.gz \
            SLIDINGWINDOW:4:20 MINLEN:125  2> $OUTDIR/$TRIMMED_DIR/quality_read_trimm_info
    fi
fi
status_trimmomatic=$?
if [ $status_trimmomatic -ne 0 ]; then
    echo "Error with Trimmomatic [$status_trimmomatic]. Invalid FASTQ format or path?" >&2
    exit 1
fi

#NOTE Hide lab and sequencing info to anonymize more strongly
# sed -e 's/^@\([A-Za-z]*\)[0-9]*:\([^:]*\):[^:]*:\(.*\):.*$/@\1000:\2:XXX:\3:1/'
zcat $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.fastq.gz            | perl -pe 'BEGIN {sub inc { my ($num) = @_; ++$num }} s/^(@).+/${1} . anonymized_header . (inc($num++))/e  if( $. %4 == 1)' >$OUTDIR/$TRIMMED_DIR/$PREFIX-r1.fastq            && rm -f $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.fastq.gz            && gzip -9 $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.fastq
zcat $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.not-paired.fastq.gz | perl -pe 'BEGIN {sub inc { my ($num) = @_; ++$num }} s/^(@).+/${1} . anonymized_header . (inc($num++))/e  if( $. %4 == 1)' >$OUTDIR/$TRIMMED_DIR/$PREFIX-r1.not-paired.fastq && rm -f $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.not-paired.fastq.gz && gzip -9 $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.not-paired.fastq
if [ "$PAIRED_END" = true ]; then
    zcat $OUTDIR/$TRIMMED_DIR/$PREFIX-r2.fastq.gz            | perl -pe 'BEGIN {sub inc { my ($num) = @_; ++$num }} s/^(@).+/${1} . anonymized_header . (inc($num++))/e  if( $. %4 == 1)' >$OUTDIR/$TRIMMED_DIR/$PREFIX-r2.fastq            && rm -f $OUTDIR/$TRIMMED_DIR/$PREFIX-r2.fastq.gz            && gzip -9 $OUTDIR/$TRIMMED_DIR/$PREFIX-r2.fastq
    zcat $OUTDIR/$TRIMMED_DIR/$PREFIX-r2.not-paired.fastq.gz | perl -pe 'BEGIN {sub inc { my ($num) = @_; ++$num }} s/^(@).+/${1} . anonymized_header . (inc($num++))/e  if( $. %4 == 1)' >$OUTDIR/$TRIMMED_DIR/$PREFIX-r2.not-paired.fastq && rm -f $OUTDIR/$TRIMMED_DIR/$PREFIX-r2.not-paired.fastq.gz && gzip -9 $OUTDIR/$TRIMMED_DIR/$PREFIX-r2.not-paired.fastq
fi


# Quality Control
echo "*** Quality Control ***"
mkdir -p $OUTDIR/$FASTQC_DIR/raw_data
mkdir -p $OUTDIR/$FASTQC_DIR/trimmed_data
if [ "$PAIRED_END" = true ]; then
    $FASTQC_EXE -t 2 -o $OUTDIR/$FASTQC_DIR/raw_data      $OUTDIR/TMP/$PREFIX-r1.fastq.gz          $OUTDIR/TMP/$PREFIX-r2.fastq.gz           > $OUTDIR/$FASTQC_DIR/raw_data/log     2>&1
    $FASTQC_EXE -t 2 -o $OUTDIR/$FASTQC_DIR/trimmed_data  $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.fastq.gz $OUTDIR/$TRIMMED_DIR/$PREFIX-r2.fastq.gz  > $OUTDIR/$FASTQC_DIR/trimmed_data/log 2>&1
else
    $FASTQC_EXE -t 2 -o $OUTDIR/$FASTQC_DIR/raw_data      $OUTDIR/TMP/$PREFIX-r1.fastq.gz                                                    > $OUTDIR/$FASTQC_DIR/raw_data/log     2>&1
    $FASTQC_EXE -t 2 -o $OUTDIR/$FASTQC_DIR/trimmed_data  $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.fastq.gz                                           > $OUTDIR/$FASTQC_DIR/trimmed_data/log 2>&1
fi


# Taxonomic sequence classification
echo "*** Taxonomic sequence classification ***"
#NOTE Kraken db from  https://ccb.jhu.edu/software/kraken/dl/minikraken_20171019_4GB.tgz
mkdir -p $OUTDIR/$CONTAMINATION_DIR
if [ $CLADE = 'virus' ]; then
    if [ "$PAIRED_END" = true ]; then
        $KRAKEN2_EXE  --db $KRAKEN2_DB --threads ${SLURM_CPUS_PER_TASK:=8} --gzip-compressed --report $OUTDIR/$CONTAMINATION_DIR/${PREFIX}_kraken_species.tab --paired $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.fastq.gz $OUTDIR/$TRIMMED_DIR/$PREFIX-r2.fastq.gz > $OUTDIR/$CONTAMINATION_DIR/${PREFIX}_sequences.kraken 2> $OUTDIR/$CONTAMINATION_DIR/${PREFIX}_sequences.kraken.log
    else
        $KRAKEN2_EXE  --db $KRAKEN2_DB --threads ${SLURM_CPUS_PER_TASK:=8} --gzip-compressed --report $OUTDIR/$CONTAMINATION_DIR/${PREFIX}_kraken_species.tab          $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.fastq.gz                                          > $OUTDIR/$CONTAMINATION_DIR/${PREFIX}_sequences.kraken 2> $OUTDIR/$CONTAMINATION_DIR/${PREFIX}_sequences.kraken.log
    fi
    status_kraken=$?
    if [ $status_kraken -ne 0 ]; then
        echo "Error with Kraken2 [$status_kraken]. Invalid thread number, database or path?" >&2
        exit 1
    fi
fi
#CLEANING: Remove Kraken output because report has already been produced!
rm -f $OUTDIR/$CONTAMINATION_DIR/${PREFIX}_sequences.kraken


# Generate report
echo "*** Generate report ***"
## percent_contamination
#provided in *_kraken_quality.tab or more globally in *_kraken_species.tab
## percent_read_length_trimming
echo '#Before trimming: forward reads (min/max/median/mean) / reverse reads (min/max/median/mean)' > $OUTDIR/$TRIMMED_DIR/trimming.stat
if [ "$PAIRED_END" = true ]; then
    zcat $OUTDIR/TMP/$PREFIX-r1.fastq.gz          | sed -n '2~4p' | awk '{print length($0)}' | $R_EXE -e 'd<-scan("stdin", quiet=TRUE);cat(min(d), max(d), median(d), mean(d), sep="\t");cat("\t")' >> $OUTDIR/$TRIMMED_DIR/trimming.stat
    zcat $OUTDIR/TMP/$PREFIX-r2.fastq.gz          | sed -n '2~4p' | awk '{print length($0)}' | $R_EXE -e 'd<-scan("stdin", quiet=TRUE);cat(min(d), max(d), median(d), mean(d), sep="\t");cat("\n")' >> $OUTDIR/$TRIMMED_DIR/trimming.stat
else
    zcat $OUTDIR/TMP/$PREFIX-r1.fastq.gz          | sed -n '2~4p' | awk '{print length($0)}' | $R_EXE -e 'd<-scan("stdin", quiet=TRUE);cat(min(d), max(d), median(d), mean(d), min(d), max(d), median(d), mean(d), sep="\t");cat("\n")' >> $OUTDIR/$TRIMMED_DIR/trimming.stat
fi
echo '#After trimming: forward reads (min/max/median/mean) / reverse reads (min/max/median/mean)' >> $OUTDIR/$TRIMMED_DIR/trimming.stat
if [ "$PAIRED_END" = true ]; then
    zcat $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.fastq.gz | sed -n '2~4p' | awk '{print length($0)}' | $R_EXE -e 'd<-scan("stdin", quiet=TRUE);cat(min(d), max(d), median(d), mean(d), sep="\t");cat("\t")' >> $OUTDIR/$TRIMMED_DIR/trimming.stat
    zcat $OUTDIR/$TRIMMED_DIR/$PREFIX-r2.fastq.gz | sed -n '2~4p' | awk '{print length($0)}' | $R_EXE -e 'd<-scan("stdin", quiet=TRUE);cat(min(d), max(d), median(d), mean(d), sep="\t");cat("\n")' >> $OUTDIR/$TRIMMED_DIR/trimming.stat
else
    zcat $OUTDIR/$TRIMMED_DIR/$PREFIX-r1.fastq.gz | sed -n '2~4p' | awk '{print length($0)}' | $R_EXE -e 'd<-scan("stdin", quiet=TRUE);cat(min(d), max(d), median(d), mean(d), min(d), max(d), median(d), mean(d), sep="\t");cat("\n")' >> $OUTDIR/$TRIMMED_DIR/trimming.stat
fi
## depth_coverage
if [ ! -f "$REF_GENOME/reference-strain.fa.stat" ]; then
    grep -v '^>' $REF_GENOME/reference-strain.fa | wc -lm | awk '{print $2-$1}' > $REF_GENOME/reference-strain.fa.stat
fi
## report
#generate_trimming-cleaning_report.pl [options] -kr=<kraken_report_file> -ts=<trimming_stat_file> -rgl=<reference_genome_length_file> -tcr=<trimmomatic_report_file> -fqcr=<fastqc_report_file> -fs=<filtered_species_name> -pcg=0.2 -pco=5 -dcg=60 -dco=30 -prltg=75 -prlto=67
$SPSP_EXE perl /usr/local/bin/generate_trimming-cleaning_report.pl -kr=$OUTDIR/$CONTAMINATION_DIR/${PREFIX}_kraken_species.tab -ts=$OUTDIR/$TRIMMED_DIR/trimming.stat -rgl=$REF_GENOME/reference-strain.fa.stat -tcr=$OUTDIR/$TRIMMED_DIR/quality_read_trimm_info -fqcr=$OUTDIR/$FASTQC_DIR/trimmed_data/$PREFIX-r1_fastqc.html -fs=$SPECIES_NAME -pcg=0.2 -pco=5 -dcg=60 -dco=30 -prltg=75 -prlto=67 > $OUTDIR/report.json


# Cleaning
rm -Rf $OUTDIR/TMP/
echo "*** DONE ***"

exit 0

