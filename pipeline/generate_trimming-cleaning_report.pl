#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use Data::Dumper;

my $VERSION = '0.4';

## Initialize and parse command line arguments
my ($help, $version)          = (0, 0);
# Input files
my ($kraken_report)           = ('');
my ($trimming_stat)           = ('');
my ($reference_genome_length) = ('');
my ($trimmomatic_report)      = ('');
my ($fastqc_report_file_f)    = ('');
# Parameters
my ($filtered_species_name)   = ('Staphylococcus aureus');
my ($pcg, $pco)               = (0.2, 5); # percent contamination threshold for green and orange status
my ($dcg, $dco)               = (60, 30); # depth coverage for green and orange status
my ($prltg, $prlto)           = (75, 67); # percent read length trimming for green and orange status

GetOptions ('help|H|?'              => sub { help(0); },
            'version|V'             => sub { print "Version: $VERSION\n"; exit 0; },
            # input files
            'kraken|kr=s'           => \$kraken_report,
            'trimming_stat|ts=s'    => \$trimming_stat,
            'ref_genome|rgl=s'      => \$reference_genome_length,
            'trimmomatic|tcr=s'     => \$trimmomatic_report,
            'fastqc_report|fqcr=s'  => \$fastqc_report_file_f,
            # parameters
            'filtered_species|fs=s' => \$filtered_species_name,
            'pcg=f'                 => \$pcg,   # percent contamination threshold for green  status
            'pco=f'                 => \$pco,   # percent contamination threshold for orange status
            'dcg=f'                 => \$dcg,   # depth coverage for green  status
            'dco=f'                 => \$dco,   # depth coverage for orange status
            'prltg=f'               => \$prltg, # percent read length trimming for green  status
            'prlto=f'               => \$prlto, # percent read length trimming for orange status
           )
           or help(1, 'Error in command line arguments');

if ( !$kraken_report           || !-e "$kraken_report"           || !-r "$kraken_report"           || !-s "$kraken_report" ){
    help(1, 'Missing kraken_report file');
}
if ( !$trimming_stat           || !-e "$trimming_stat"           || !-r "$trimming_stat"           || !-s "$trimming_stat" ){
    help(1, 'Missing trimming_stat file');
}
if ( !$reference_genome_length || !-e "$reference_genome_length" || !-r "$reference_genome_length" || !-s "$reference_genome_length" ){
    help(1, 'Missing reference_genome_length file');
}
if ( !$trimmomatic_report      || !-e "$trimmomatic_report"      || !-r "$trimmomatic_report"      || !-s "$trimmomatic_report" ){
    help(1, 'Missing trimmomatic_report file');
}
if ( !$fastqc_report_file_f    || !-e "$fastqc_report_file_f"    || !-r "$fastqc_report_file_f"    || !-s "$fastqc_report_file_f" ){
    help(1, 'Missing fastqc_report_file file');
}
$filtered_species_name =~ s{_}{ }g;

my $json;


# percent_contamination
# Percent reads that map to different bacterial order/class based on KRAKEN output
my $sum   = 0;
my $match = 0;
open (my $KRAKEN, '<', "$kraken_report")  or die "Cannot open Kraken report file [$kraken_report]\n";
while (<$KRAKEN>){
    my (undef, $read_count, undef, $species_tag, undef, $species_name) = split(/\t/, $_);
    if ( $species_tag eq 'S' ){#TODO Kraken2 has also S1 species tag ???
        $sum += $read_count;
    }
    if ( $species_name =~ /\s$filtered_species_name$/i ){
        $match = $read_count;
    }
}
close $KRAKEN;
#In case no species tag, e.g. all unclassified
if ( $sum == 0 ){
    $json->{'percent_contamination'} = 100;
}
else {
    $json->{'percent_contamination'} = 100 - (100*$match/$sum);
}


# percent_read_length_trimming
# Median read length after trimming divided by median read length before trimming [%]
open (my $TRIMMING, '<', "$trimming_stat")  or die "Cannot open trimming stat file [$trimming_stat]\n";
my $before_done = 0;
while(<$TRIMMING>){
    if ( ! /^#/ ){
        chomp $_;
        my ($min_f, $max_f, $median_f, $mean_f, $min_r, $max_r, $median_r, $mean_r) = split(/\t/, $_);
        my $name = 'before_trimming_';
        if ( $before_done ){
            $name = 'after_trimming_';
        }
        $json->{'QC_stat'}->{$name.'min_forward'}    = $min_f;
        $json->{'QC_stat'}->{$name.'max_forward'}    = $max_f;
        $json->{'QC_stat'}->{$name.'median_forward'} = $median_f;
        $json->{'QC_stat'}->{$name.'mean_forward'}   = $mean_f;
        $json->{'QC_stat'}->{$name.'min_reverse'}    = $min_r;
        $json->{'QC_stat'}->{$name.'max_reverse'}    = $max_r;
        $json->{'QC_stat'}->{$name.'median_reverse'} = $median_r;
        $json->{'QC_stat'}->{$name.'mean_reverse'}   = $mean_r;

        my ($min, $max, $median, $mean);
        if ( $min_f == 0 || $min_r == 0 ){
            warn "\tIssue with same FASTQ file within [$trimming_stat]\n";
            $min = $min_f > 0 ? $min_f : $min_r;
        }
        else {
            $min = $min_f < $min_r ? $min_f : $min_r;
        }
        $max    = $max_f    > $max_r    ? $max_f    : $max_r;
        $median = $median_f < $median_r ? $median_f : $median_r;
        $mean   = $mean_f   < $mean_r   ? $mean_f   : $mean_r;
        $json->{'QC_stat'}->{$name.'min'}    = $min;
        $json->{'QC_stat'}->{$name.'max'}    = $max;
        $json->{'QC_stat'}->{$name.'median'} = $median;
        $json->{'QC_stat'}->{$name.'mean'}   = $mean;
        $before_done = 1;
    }
}
$json->{'percent_read_length_trimming'} = 100 * ($json->{'QC_stat'}->{'after_trimming_median'} / $json->{'QC_stat'}->{'before_trimming_median'});
close $TRIMMING;


# depth_coverage
# Average depth (computed as number of reads * observed median read length / genome length) [X]        ---------> after trimming!
open (my $REF_LENGTH, '<', "$reference_genome_length")  or die "Cannot open reference genome length file [$reference_genome_length]\n";
my $ref_genome_length = 0;
while(<$REF_LENGTH>){
    $ref_genome_length = $_;
    chomp $ref_genome_length;
}
close $REF_LENGTH;

open (my $TRIMMOMATIC_REPORT, '<', "$trimmomatic_report")  or die "Cannot open Trimmomatic report file [$trimmomatic_report]\n";
my $read_number = 0;
while(<$TRIMMOMATIC_REPORT>){
    if ( /Both Surviving:\s*(\d+)/ ){
        $read_number = $1;
        last;
    }
}
close $TRIMMOMATIC_REPORT;
$json->{'depth_coverage'} = $read_number * $json->{'QC_stat'}->{'after_trimming_median'} / $ref_genome_length;


# FastQC files
if ( -e "$fastqc_report_file_f" && -s "$fastqc_report_file_f" ){
    push @{ $json->{'FastQC_report_files'} }, "$fastqc_report_file_f";
}
my $fastqc_report_file_r = $fastqc_report_file_f;
$fastqc_report_file_r =~ s{\-r1_fastqc\.html$}{-r2_fastqc.html};
if ( -e "$fastqc_report_file_r" && -s "$fastqc_report_file_r" ){
    push @{ $json->{'FastQC_report_files'} }, "$fastqc_report_file_r";
}


# quality_status
# Sample quality/zone: Green zone ("accepted without user intervention") | Orange zone ("acceptance based on user") | Red zone ("discarded")
$json->{'quality_status'} = 'red';
# green
if (    $json->{'percent_contamination'} <= $pcg && $json->{'depth_coverage'} >= $dcg && $json->{'percent_read_length_trimming'} >= $prltg ){
    $json->{'quality_status'} = 'green';
}
# orange
elsif ( $json->{'percent_contamination'} <= $pco && $json->{'depth_coverage'} >= $dco && $json->{'percent_read_length_trimming'} >= $prlto ){
    $json->{'quality_status'} = 'orange';
}


# JSON output (from Data::Dumper !!!)
$Data::Dumper::Sortkeys = 1;
$Data::Dumper::Useqq    = 1;
$Data::Dumper::Indent   = 0;
my $dump = Dumper $json;
$dump =~ s{^\$VAR1\s*=\s*}{};
$dump =~ s{\s*=>\s*}{:}g;
$dump =~ s{;$}{};
print $dump;

exit 0;


sub help {
    my ($exit_code, $msg) = @_;

    if ( $exit_code != 0 ){
        *STDOUT = *STDERR;
    }

    if ( $msg ){
        print "\n\t$msg\n\n";
    }
    print <<"EOF";
Version: $VERSION

$0 [options] -kr=<kraken_report_file> -ts=<trimming_stat_file> -rgl=<reference_genome_length_file> -tcr=<trimmomatic_report_file> -fqcr=<fastqc_report_file>

General options:
         -fs               filtered species name (default: "$filtered_species_name")
         -pcg              percent contamination threshold for green  status (default: "$pcg")
         -pco              percent contamination threshold for orange status (default: "$pco")
         -dcg              depth coverage for green  status (default: "$dcg")
         -dco              depth coverage for orange status (default: "$dco")
         -prltg            percent read length trimming for green  status (default: "$prltg")
         -prlto            percent read length trimming for orange status (default: "$prlto")
         -h                print this help
         -V                print version information
EOF
    exit $exit_code;
}
