#!/bin/sh

#SBATCH --export=NONE
#SBATCH --mem=50G
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --job-name=comparative_genomics-Global-ST
#TODO adjust time scheduler, currently "days-hours:minutes:seconds"
#SBATCH --time=7-00:00:00


BIND=/data

# Commands
#singularity exec --bind <data_path if not run from /tmp or /home> /software/singularity/containers/Projects/SPSP
SINGULARITY_PATH="singularity exec --bind $BIND /software/singularity/containers/Projects/SPSP"

GUBBINS_EXE="$SINGULARITY_PATH/Gubbins-2.3.4-1.ubuntu.simg run_gubbins.py"


MLST_DIR=MLST


usage() { echo "Usage: $0 [-r specimen_analyses directory] {-c clade} {-t taxId}" 1>&2; exit 1; }
while getopts ":r:c:t:" option; do
    case "${option}" in
        r)
            r=${OPTARG}
            ;;
        c)
            c=${OPTARG}
            ;;
        t)
            t=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))
if [ -z "${r}" ]; then
    usage
fi


# Default is Bacteria
CLADE="bacteria"
if [ -n "${c}" ]; then
    CLADE=${c}
fi


# Set default species related to clade
if [ $CLADE = 'bacteria' ]; then
    #NOTE S. aureus taxid
    SPECIES=1280
    BIND="$BIND/bacteria"
else
    echo "Invalid clade [${c}]" 1>&2
    exit 1
fi


if [ -n "${t}" ]; then
    SPECIES=${t}
fi
GLOBAL=$BIND/processed_data/comparative_analyses/species_based/$SPECIES/global_tree
ST=$BIND/processed_data/comparative_analyses/species_based/$SPECIES/ST_trees


# Create reference tree (global tree)
echo "*** Create reference tree ***"
mkdir -p $GLOBAL
cat ${r}/*/$MLST_DIR/reference/*_on_reference.fasta > $GLOBAL/reference_align.fasta
cd $GLOBAL
$GUBBINS_EXE --threads ${SLURM_CPUS_PER_TASK:=8} *_align.fasta
ln -fs reference_align.final_tree.tre reference_align.final_tree.nwk
cd - >/dev/null 2>&1


# Prepare Sequence Type trees
echo "*** Prepare Sequence Type trees ***"
mkdir -p $ST
for i in ${r}/*/$MLST_DIR/ST-*/; do
    sequence_type=$(echo "$i" | awk -F"ST-" '{ print $2}' | awk -F"/" '{ print $1}')
    echo mkdir -p $ST/ST-$sequence_type
    echo cat "${r}/*/$MLST_DIR/ST-"$sequence_type"/*.fasta > $ST/ST-$sequence_type/ST-"$sequence_type"_align.fasta"
done | sort -u -r > $ST/prepare_sequence_type.sh

sh $ST/prepare_sequence_type.sh


# Run Sequence Type tree (local (ST) tree)
echo "*** Run Sequence Type tree ***"
rm -f $ST/warnings
for i in $ST/ST-*; do
    cd "$i"
    # gubbins needs more than 3 strains
    if [ $(grep -c ">" *_align.fasta) -gt 3 ]; then
        $GUBBINS_EXE --threads ${SLURM_CPUS_PER_TASK:=8} *_align.fasta
        nwk=`ls *_align.final_tree.tre`
        ln -fs $nwk ${nwk/.tre/.nwk}
    else
        name=`basename $i`
        echo "Less than 3 strains for $name" >> $ST/warnings
    fi
    cd - >/dev/null 2>&1
done

echo "*** DONE ***"
exit 0

