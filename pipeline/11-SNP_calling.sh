#!/bin/bash

#SBATCH --export=NONE
#SBATCH --mem=50G
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --job-name=snp_calling

########################################
# - ALIGNMENT and READ MAPPING
# - VARIANT CALL
# - CONSENSUS CALL (= clean assembly)
########################################

export LC_ALL="C"
BIND=/data

# Commands
#singularity exec --bind <data_path if not run from /tmp or /home> /software/singularity/containers/Projects/SPSP
SINGULARITY_PATH="singularity exec --bind $BIND /software/singularity/containers/Projects/SPSP"

# ALIGNMENT and READ MAPPING
BWA_EXE="$SINGULARITY_PATH/BWA-0.7.17-1.ubuntu.simg bwa"
SAMTOOLS_EXE="$SINGULARITY_PATH/samtools-1.9-1.biocontainers.simg samtools"
VARIANTBAM_EXE="$SINGULARITY_PATH/VariantBam-b797b78-1.ubuntu18.sif variant"
# VARIANT CALL
PILON_EXE="$SINGULARITY_PATH/pilon-1.23-1.ubuntu18.sif pilon"
VCFLIB_EXE="$SINGULARITY_PATH/vcflib-1.0.0_rc1-1.biocontainers.simg vcffilter"
#SNPEFF_EXE="$SINGULARITY_PATH/SnpEff-4.3t-1.ubuntu18.sif snpEff"
# CONSENSUS CALL (= clean assembly)
BEDTOOLS_EXE="$SINGULARITY_PATH/BEDTools-2.27.1-1.ubuntu18.sif bedtools"
BCFTOOLS_EXE="$SINGULARITY_PATH/bcftools_1.9--h4da6232_0.sif bcftools"
BGZIP_EXE="$SINGULARITY_PATH/VariantBam-b797b78-1.ubuntu18.sif bgzip"
TABIX_EXE="$SINGULARITY_PATH/VariantBam-b797b78-1.ubuntu18.sif tabix"
#SEQTK_EXE="$SINGULARITY_PATH/seqtk-1.2.r94-1.ubuntu18.sif seqtk"


MLST_DIR=MLST
#ASSEMBLY_DIR=assembly
MAPPING_DIR=Mapping
VARIANT_DIR=variantcall


usage() { echo -e "Usage: $0 [-f trimmed_forward_read_file] [-r trimmed_reverse_read_file] {-s sampleId_to_use} {-c virus} {-t taxId} {-o output_folder}\n\tor\n       $0 [-f trimmed_unpaired_read_file]                               {-s sampleId_to_use} {-c virus} {-t taxId} {-o output_folder}\n\tor\n       $0 [-m metagenome_fasta_file]                                    {-s sampleId_to_use} {-c virus} {-t taxId} {-o output_folder}" 1>&2; exit 1; }
while getopts ":f:r:m:s:c:t:o:" option; do
    case "${option}" in
        f)
            f=${OPTARG}
            ;;
        r)
            r=${OPTARG}
            ;;
        m)
            m=${OPTARG}
            ;;
        s)
            s=${OPTARG}
            ;;
        c)
            c=${OPTARG}
            ;;
        t)
            t=${OPTARG}
            ;;
        o)
            o=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))
METAGENOMICS=false
PAIRED_END=true
if [ -z "${f}" ] && [ -z "${m}" ]; then
    usage
fi
#Force -s option
if [ -z "${s}" ]; then
    usage
fi
if [ -z "${r}" ]; then
    PAIRED_END=false
fi
if [ -n "${m}" ]; then
    METAGENOMICS=true
fi


# Default is Virus
CLADE="virus"
if [ -n "${c}" ]; then
    CLADE=${c}
fi


if [ $CLADE = 'virus' ]; then
    #NOTE SARScov2 taxid
    SPECIES=2697049
    BIND="$BIND/viruses"
else
    echo "Invalid clade [${c}]" 1>&2
    exit 1
fi


if [ -n "${t}" ]; then
    SPECIES=${t}
fi
REF_GENOME=$BIND/reference_data/$SPECIES/reference_genome/reference.fasta
VARIANTBAM_EXT_FILES=$BIND/reference_data/$SPECIES/variantbam
ALLELE_EXT_FILE=$BIND/reference_data/$SPECIES/allele/SARS-coV-2_mlst.txt

OUTPUT="."
if [ -n "${o}" ]; then
    OUTPUT=${o}
fi


# Common prefix between forward and reverse read files
if [ "$PAIRED_END" = true ] && [ "$METAGENOMICS" = false ]; then
    PREFIX=`printf "%s\n%s\n" "${f}" "${r}" | sed -e 'N;s/^\(.*\).*\n\1.*$/\1/' | xargs basename | sed -e 's|[-_][Rr]$||'` # Remove trailing delimiter for forward/reverse paired-end reads
elif [ "$PAIRED_END" = false ] && [ "$METAGENOMICS" = false ]; then
    PREFIX=`printf "%s\n"     "${f}"        | sed -e 'N;s/^\(.*\).*\n\1.*$/\1/' | xargs basename | sed -e 's|[-_][Rr]$||'` # Remove trailing delimiter for forward/reverse paired-end reads
elif [ "$METAGENOMICS" = true ]; then
    PREFIX=`printf "%s\n"     "${m}"        | sed -e 'N;s/^\(.*\).*\n\1.*$/\1/' | xargs basename | sed -e 's|_.*$||'` # Remove trailing delimiter for forward/reverse paired-end reads + lab id
fi
#Force prefix from command line
if [ -n "${s}" ]; then
    PREFIX=${s}
fi
OUTDIR=$OUTPUT/$PREFIX

# Create result folder
mkdir -p $OUTDIR



# ALIGNMENT and READ MAPPING
#dependencies needed: Json files containing mapping and unmapping rules + reference.fasta
#Summary:
#-The reads are mapped against the chosen reference (MN908947.3 -Wuhan1),
#-further converted into bam format for later handling, and sorted.
#-The alignments are then screened for mapping and unmapping reads using variantbam
#-Statistics are outputted concerning the ratio of mapped and unmapped reads in a dedicated file
#-Resorting of the filtered bam
#-Selective downsampling of high coverage regions (>1000 reads per section) for graphical purpose only later on, to better identify droputs
#-Coverge stats are outputted as well
#NOTE Typing is currently only for bacteria, so set always "ST-0" for viruses
sequencetype="ST-0"
if [ "$METAGENOMICS" = true ]; then
    #fake variant call
    mkdir -p $OUTDIR/$VARIANT_DIR/$sequencetype
    # For compatibility with Bacteria
    ln -s $VARIANT_DIR $OUTDIR/$MLST_DIR
else
    mkdir -p $OUTDIR/$MAPPING_DIR
    cp $REF_GENOME $OUTDIR/$MAPPING_DIR/reference.fa
    echo "*** Alignment and read mapping ***"
    #Step1: Aligning the reads to the reference
    #NOTE Indexing could be done only once. But then warning of reference updates!
    $BWA_EXE index $OUTDIR/$MAPPING_DIR/reference.fa > $OUTDIR/$MAPPING_DIR/bwa_index.log 2>&1
    if [ "$PAIRED_END" = true ]; then
        $BWA_EXE mem -t ${SLURM_CPUS_PER_TASK:=8} $OUTDIR/$MAPPING_DIR/reference.fa "${f}" "${r}" > $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.sam 2> $OUTDIR/$MAPPING_DIR/bwa_mem.log
    else
        $BWA_EXE mem -t ${SLURM_CPUS_PER_TASK:=8} $OUTDIR/$MAPPING_DIR/reference.fa "${f}"        > $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.sam 2> $OUTDIR/$MAPPING_DIR/bwa_mem.log
    fi

    #Step2: Conversion from Sam into bam, indexing and sorting
    $SAMTOOLS_EXE sort -@ ${SLURM_CPUS_PER_TASK:=8} -T $OUTDIR/$MAPPING_DIR/temp_sort -o $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.bam $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.sam > $OUTDIR/$MAPPING_DIR/samtools_sort1.log 2>&1
    $SAMTOOLS_EXE rmdup $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.bam $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.bam > $OUTDIR/$MAPPING_DIR/samtools_rmdup1.log 2>&1
    $SAMTOOLS_EXE index $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.bam > $OUTDIR/$MAPPING_DIR/samtools_index1.log 2>&1

    #Step3: Map filter, discriminating the mapping from the unmapping reads
    $VARIANTBAM_EXE $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.bam --num-threads ${SLURM_CPUS_PER_TASK:=8} -r $VARIANTBAM_EXT_FILES/map_rules.json   -v > $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.mapped.reads.only.sam   2> $OUTDIR/$MAPPING_DIR/variant_map.log
    #$VARIANTBAM_EXE $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.bam --num-threads ${SLURM_CPUS_PER_TASK:=8} -r $VARIANTBAM_EXT_FILES/unmap_rules.json -v > $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.unmapped.reads.only.sam 2> $OUTDIR/$MAPPING_DIR/variant_unmap.log

    #Step4: Mapping stats
    #NOTE Skipped, we don't use it yet

    #Step5: Resorting and indexing
    #NOTE only "alignment.removed.duplicates.mapped.reads.only.sorted.bam" is used afterwards for calling variants
    $SAMTOOLS_EXE sort -@ ${SLURM_CPUS_PER_TASK:=8} -T $OUTDIR/$MAPPING_DIR/temp_sort $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.mapped.reads.only.sam   -o $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.mapped.reads.only.sorted.bam   > $OUTDIR/$MAPPING_DIR/samtools_sort2m.log 2>&1
    #$SAMTOOLS_EXE sort -@ ${SLURM_CPUS_PER_TASK:=8} -T $OUTDIR/$MAPPING_DIR/temp_sort $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.unmapped.reads.only.sam -o $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.unmapped.reads.only.sorted.bam > $OUTDIR/$MAPPING_DIR/samtools_sort2u.log 2>&1
    $SAMTOOLS_EXE index $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.mapped.reads.only.sorted.bam   > $OUTDIR/$MAPPING_DIR/samtools_index2m.log 2>&1
    #$SAMTOOLS_EXE index $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.unmapped.reads.only.sorted.bam > $OUTDIR/$MAPPING_DIR/samtools_index2u.log 2>&1

    #Step6: Selective downsampling anf the alignments only for the regions displaying coverage > 1000 bp per section, sorting of it and indexing for later usage
    #NOTE Skipped, we don't use it yet
    #Step7: Coverage stats are being outputted
    #NOTE Skipped, we don't use it yet



    # VARIANT CALL
    #dependencies needed: secondary_revealer1.sh, SnpEFF SARS genome repository
    #Summary:
    #-Pilon calls the variants applying no particular quality or information filter
    #-For the primary variants: vcffilter individuates SNPs applying a cutoff of AF>0.7, DP>50, parallely it individuates the deletions and insertion with the DEL/INS filter, grep eleiminates the duplicates
    #-For the seconday alleles: vcffilter creates a file with only AF>0 are collected -this step is done to speed up the process-
    #-the bash script secondary_revealer1.sh loops through the AF>0 vcf and identifies the secondary alleles at a given position with an 0<AF<0.99, outputs in the "sample_id".secondary_alleles.vcf and report.tech
    #-finally snpeff annotates the primary and the secondary alleles
    echo "*** Variant calling ***"
    #Step1: variant call
    mkdir -p $OUTDIR/$VARIANT_DIR/$sequencetype
    # For compatibility with Bacteria
    ln -s $VARIANT_DIR $OUTDIR/$MLST_DIR
    $PILON_EXE --threads ${SLURM_CPUS_PER_TASK:=8} --genome $OUTDIR/$MAPPING_DIR/reference.fa --frags $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.mapped.reads.only.sorted.bam --changes --variant --outdir $OUTDIR/$VARIANT_DIR/ --output ${PREFIX} > $OUTDIR/$VARIANT_DIR/pilon.log 2>&1

    #Step2: variant filtering to identify primary alleles
    $VCFLIB_EXE -f "SVTYPE = DEL & ! IMPRECISE" --or -f "SVTYPE = INS & ! IMPRECISE" --or -f "AF > 0.7 & DP > 50" $OUTDIR/$VARIANT_DIR/${PREFIX}.vcf > $OUTDIR/$VARIANT_DIR/${PREFIX}.depth.filtered.vcf 2> $OUTDIR/$VARIANT_DIR/vcffilter.log
    grep -v '<DUP>' $OUTDIR/$VARIANT_DIR/${PREFIX}.depth.filtered.vcf > $OUTDIR/$VARIANT_DIR/${PREFIX}.final.vcf

    #Step3: variant filtering and identification of secondary alleles
    #NOTE Skipped, we don't use it yet
    #Step4: primary and secondary variant annotation
    #NOTE Skipped, we don't use it yet



    # CONSENSUS CALL (= clean assembly)
    #dependencies needed: none
    #Summary:
    #-First create the bedgraph files of the converage of both the subsampled and the native bam files, it will be used later for plotting
    #-Creation of a bedfiles featuring the low coverage areas set as < 50
    #-Subtraction from this bedfile of the position of the variants (they have anyway more than 50 reads to be called), this is done to include deletions, otherwiseregarded as regions with 0 coverage. -finalmask
    #-Compression and indexing of the variant file
    #-Creation of the consensus from the variant file, the reference, and the final mask featuring low coverage regions. Those regions will be called N
    #-Calculation of the N percentage per sequence, this is later useful to tag the samples as High coverage (%N<10) or Low coverage (%N>10)
    #-File tagging
    #-Renaming of the header of the consensus sequence with the samplename
    echo "*** Consensus calling ***"
    #Step1: plotting the coverage
    #NOTE Skipped, we don't use it yet
    #Step2: identifying a first N mask for low coverage and then correcting it for the deletions
    $BEDTOOLS_EXE genomecov -bga -ibam $OUTDIR/$MAPPING_DIR/${PREFIX}.alignment.removed.duplicates.mapped.reads.only.sorted.bam | awk '$4<50' > $OUTDIR/$VARIANT_DIR/${PREFIX}.lessthan50.bed
    $BEDTOOLS_EXE subtract -a $OUTDIR/$VARIANT_DIR/${PREFIX}.lessthan50.bed -b $OUTDIR/$VARIANT_DIR/${PREFIX}.final.vcf > $OUTDIR/$VARIANT_DIR/${PREFIX}.finalmask.bed 2> $OUTDIR/$VARIANT_DIR/bedtools_subtract.log

    #Step3: compression of variantfile -it is here and not before becuase of the incompatibility of BCF modules and vcflib modules
    $BGZIP_EXE -c -i $OUTDIR/$VARIANT_DIR/${PREFIX}.final.vcf > $OUTDIR/$VARIANT_DIR/${PREFIX}.consvariants.vcf.gz
    $TABIX_EXE -p vcf $OUTDIR/$VARIANT_DIR/${PREFIX}.consvariants.vcf.gz -f > $OUTDIR/$VARIANT_DIR/${PREFIX}.consvariants.vcf.gz.tbi 2> $OUTDIR/$VARIANT_DIR/tabix.log

    #Step4: consensus calling
    $BCFTOOLS_EXE consensus $OUTDIR/$VARIANT_DIR/${PREFIX}.consvariants.vcf.gz -f $OUTDIR/$MAPPING_DIR/reference.fa -m $OUTDIR/$VARIANT_DIR/${PREFIX}.finalmask.bed -o $OUTDIR/$VARIANT_DIR/${PREFIX}.old.consensus.fasta 2> $OUTDIR/$VARIANT_DIR/bcftools_consensus.log

    #Step5: Elaborating the stats on the Ns
    #NOTE Skipped, we don't use it yet
    #Step6: Sample tagging: classify the sequence as bad or good based on the percentage of Ns, <10% will be considered good, >10% will be considered trash, however, no sequence will be dropped
    #NOTE Skipped, we don't use it yet
    #Step7: change the name of the header of the consensus to the sample name and not the reference
    #NOTE Do not use sampletag defined in Step6 here
    sed -e "s|MN908947\.3|${PREFIX}|g" $OUTDIR/$VARIANT_DIR/${PREFIX}.old.consensus.fasta > $OUTDIR/$VARIANT_DIR/${PREFIX}.consensus.fasta
    gzip -9 $OUTDIR/$VARIANT_DIR/${PREFIX}.old.consensus.fasta
    #rm -f $OUTDIR/$VARIANT_DIR/${PREFIX}.old.consensus.fasta
fi
# For compatibility with Bacteria
ln -fs ../${PREFIX}.consensus.fasta $OUTDIR/$VARIANT_DIR/$sequencetype/${PREFIX}_on_${sequencetype}.fasta
cp $ALLELE_EXT_FILE $OUTDIR/$VARIANT_DIR/${PREFIX}_mlst.txt
sed -i "s@__SPL__@${PREFIX}@" $OUTDIR/$VARIANT_DIR/${PREFIX}_mlst.txt


#CLEANING:
#TODO

echo "*** DONE ***"
exit 0

