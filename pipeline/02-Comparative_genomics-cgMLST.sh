#!/bin/sh

#SBATCH --export=NONE
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --job-name=comparative_genomics-cgMLST
#TODO adjust time scheduler, currently "days-hours:minutes:seconds"
#SBATCH --time=7-00:00:00


export LC_ALL="C"
BIND=/data/bacteria

# Commands
#singularity exec --bind <data_path if not run from /tmp or /home> /software/singularity/containers/Projects/SPSP
SINGULARITY_PATH="singularity exec --bind $BIND /software/singularity/containers/Projects/SPSP"

SPSP_EXE="$SINGULARITY_PATH/SPSPscripts-2021.01.13-1.ubuntu18.sif"

PROJECT=$BIND/processed_data/comparative_analyses/project_based
MLST_DIR=MLST


usage() { echo "Usage: $0 [-r specimen_analyses directory] [-l laboratory] [-p project_name] {-s list_of_specimenID_space_separated}" 1>&2; exit 1; }
while getopts ":r:l:p:s:" option; do
    case "${option}" in
        r)
            r=${OPTARG}
            ;;
        l)
            l=${OPTARG}
            ;;
        p)
            p=${OPTARG}
            ;;
        s)
            s=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))
if [ -z "${r}" ] || [ -z "${l}" ] || [ -z "${p}" ]; then
    usage
fi

specimenIDs=`ls -d1 ${r}/* | sed -e 's@^.*/@@' | perl -pe 's@\n@ @'`
if [ -n "${s}" ]; then
    specimenIDs=${s}
fi



# Create cgMLST tree (project tree)
echo "*** Create cgMLST tree ***"
mkdir -p $PROJECT/${l}/${p}/cgMLST
rm -f $PROJECT/${l}/${p}/cgMLST/all_cgMLST.tab
for dir in ${specimenIDs}
do
    tail -n 1 ${r}/${dir}/$MLST_DIR/*_cgmlst.txt >>$PROJECT/${l}/${p}/cgMLST/all_cgMLST.tab
done
$SPSP_EXE python3 /usr/local/bin/make_njtree.py $PROJECT/${l}/${p}/cgMLST/all_cgMLST.tab > $PROJECT/${l}/${p}/cgMLST/cgMLST_tree.nwk


echo "*** DONE ***"
exit 0

