#!/bin/sh
#SBATCH --job-name=test_JOB
#SBATCH --time=7-00:00:00
#SBATCH --mem=50G
#SBATCH --qos=1week
#SBATCH --cpus-per-task=8

ml purge
ml Miniconda2/4.3.30
source activate gubbins

# Create cgMLST tree
mkdir cgMLST
cat ../../pipeline/results/*/typing/*_cgmlst.txt > cgMLST/all_cgMLST.tab
python  1NJTree/make_njtree.py cgMLST/all_cgMLST.tab > cgMLST/cgMLST_tree.nwk


# Create reference tree
mkdir -p reference

cat ../../pipeline/results/*/reference/*_on_reference.fasta > reference/reference_align.fasta
cd reference
run_gubbins.py --threads "$SLURM_CPUS_PER_TASK" *_align.fasta
cd ..

# Prepare Sequence Type trees
for i in ../../pipeline/results/*/ST-*/; do
sequence_type=$(echo "$i" | awk -F"ST-" '{ print $2}' | awk -F"/" '{ print $1}')
echo mkdir -p ST-$sequence_type
echo cat "../../pipeline/results/*/ST-"$sequence_type"/*.fasta > ST-$sequence_type/ST-"$sequence_type"_align.fasta"
done | sort -u -r > prepare_sequence_type.sh

sh prepare_sequence_type.sh

# Run Sequence Type tree
for i in ST-*
do

cd "$i"

if [ $(grep -c ">" *_align.fasta) -gt 3 ] # gubbins needs more than 3 strains
then

run_gubbins.py --threads "$SLURM_CPUS_PER_TASK" *_align.fasta

fi

cd ..

done

