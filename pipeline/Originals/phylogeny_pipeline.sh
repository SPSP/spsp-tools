#!/bin/bash

#SBATCH --qos=6hours
#SBATCH --mem=50G
#SBATCH --cpus-per-task=16
#SBATCH --job-name=snp_calling
#SBATCH --array=1-50

array=(mock run185-atcc-49775 607887-15 800236-15 400871-16 602191-17-B 401130-15 501030-17-B 500190-15 801826-17 702924-15 504117-15 140662-17 611230-17 608367-16 501058-15 504085-17 806392-15 801866-17-B 608016-16 208880-15 604392-16 612085-15 808328-17 401896-17 126616-16 500785-15 801777-15 402240-15 503342-15 807279-17 504202-15 202580-16 807990-16 601251-16 611238-17 803303-15 709802-17 602701-16 803364-17 401188-15 502776-15 502312-17 611177-17 803263-15 806968-17 602220-16 608718-16 607090-17 802643-15 607651-17)

reference=MLST/genomes/ST-genomes/reference_strain.fa

ml purge
ml Miniconda2/4.3.30
source activate spsp_py36

export _JAVA_OPTIONS="-Xmx10g"

export sample_id=${array["$SLURM_ARRAY_TASK_ID"]}

GATKPATH="/scicore/home/egliadr/wuethrda/projects/p00032_spsp/GATK/GenomeAnalysisTK-3.8-1-0-gf15c1c3ef"

PICARDPATH="/scicore/home/egliadr/wuethrda/projects/p00032_spsp/GATK/picard"

raw_R1=../reads/"$sample_id"_R1.fastq.gz
raw_R2=../reads/"$sample_id"_R2.fastq.gz
#R1=../reads/"$sample_id"_R1.fastq.gz
#R2=../reads/"$sample_id"_R2.fastq.gz

#Trimm reads
mkdir -p results/"$sample_id"/0_trimming
trimmomatic PE -threads "$SLURM_CPUS_PER_TASK" -phred33 "$raw_R1" "$raw_R2" results/"$sample_id"/0_trimming/r1.fastq.gz results/"$sample_id"/0_trimming/r1.not-paired.fastq.gz results/"$sample_id"/0_trimming/r2.fastq.gz results/"$sample_id"/0_trimming/r2.not-paired.fastq.gz ILLUMINACLIP:/scicore/home/egliadr/GROUP/Software/scripts/assembly_pipeline/NexteraPE-PE.fa:2:30:10 SLIDINGWINDOW:4:12 MINLEN:100 2> results/"$sample_id"/0_trimming/quality_read_trimm_info
R1=results/"$sample_id"/0_trimming/r1.fastq.gz
R2=results/"$sample_id"/0_trimming/r2.fastq.gz

source deactivate
source activate MLST

mkdir -p results/"$sample_id"/typing/

mentalist call -o results/"$sample_id"/typing/"$sample_id"_mlst.txt --db MLST/mlstdb/staaur_mlst.db -1 "$R1" -2 "$R2"
mentalist call -o results/"$sample_id"/typing/"$sample_id"_cgmlst.txt  --output_votes --output_special  --db MLST/cgmlstdb/staaur_cgmlst.db -1 "$R1" -2 "$R2"

sequencetype="$(awk 'END {print $9}' results/"$sample_id"/typing/"$sample_id"_mlst.txt)"

source deactivate
source activate SPAdes
mkdir -p results/"$sample_id"/
spades.py --cov-cutoff auto --careful --mismatch-correction -t "$SLURM_CPUS_PER_TASK" -k 21,33,55,77,99,127 -1 "$R1" -2 "$R2" -o results/"$sample_id"/spades_assembly

if [ ! -f MLST/genomes/ST-genomes/ST-"$sequencetype"-*.fa ] ; then

cp results/"$sample_id"/spades_assembly/scaffolds.fasta MLST/genomes/ST-genomes/ST-"$sequencetype"-"$sample_id".fa

fi



source deactivate
source activate spsp_py36

function mapping {

#index Reference
mkdir -p results/"$sample_id"/"$1"/"$2"
cp "$3" results/"$sample_id"/"$1"/"$2"/reference.fa
samtools faidx results/"$sample_id"/"$1"/"$2"/reference.fa
java -jar "$PICARDPATH"/picard.jar CreateSequenceDictionary R=results/"$sample_id"/"$1"/"$2"/reference.fa O=results/"$sample_id"/"$1"/"$2"/reference.dict
bwa index results/"$sample_id"/"$1"/"$2"/reference.fa

#map reads and format bam
bwa mem -t "$SLURM_CPUS_PER_TASK" results/"$sample_id"/"$1"/"$2"/reference.fa "$R1" "$R2" > results/"$sample_id"/"$1"/"$2"/"$sample_id".sam
samtools sort -@ "$SLURM_CPUS_PER_TASK" -T results/"$sample_id"/"$1"/"$2"/_temp -o results/"$sample_id"/"$1"/"$2"/"$sample_id"_sorted.bam results/"$sample_id"/"$1"/"$2"/"$sample_id".sam
samtools rmdup -S results/"$sample_id"/"$1"/"$2"/"$sample_id"_sorted.bam results/"$sample_id"/"$1"/"$2"/"$sample_id"_sorted.rmdup.sorted.bam
java -jar "$PICARDPATH"/picard.jar AddOrReplaceReadGroups I=results/"$sample_id"/"$1"/"$2"/"$sample_id"_sorted.rmdup.sorted.bam O=results/"$sample_id"/"$1"/"$2"/"$sample_id"_sorted.rmdup.grouped.bam RGID="$sample_id" RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=20
samtools index results/"$sample_id"/"$1"/"$2"/"$sample_id"_sorted.rmdup.grouped.bam
"$folder_name"
}

function call_variants {


#call variants
mapping "$1" 1_SNPs MLST/genomes/ST-genomes/"$1"-*.fa
java -jar "$GATKPATH"/GenomeAnalysisTK.jar -T HaplotypeCaller -ploidy 1 -R results/"$sample_id"/"$1"/1_SNPs/reference.fa -I results/"$sample_id"/"$1"/1_SNPs/"$sample_id"_sorted.rmdup.grouped.bam  -o results/"$sample_id"/"$1"/1_SNPs/"$sample_id"_sorted.rmdup.grouped.vcf.gz
vcftools --gzvcf results/"$sample_id"/"$1"/1_SNPs/"$sample_id"_sorted.rmdup.grouped.vcf.gz --remove-indels --recode --recode-INFO-all --stdout | vcffilter -f " DP  > 9 & AF > 0.9 " > results/"$sample_id"/"$1"/1_SNPs/"$sample_id"_sorted.rmdup.grouped.filtered.snps.vcf
java -jar "$GATKPATH"/GenomeAnalysisTK.jar -T FastaAlternateReferenceMaker -R results/"$sample_id"/"$1"/1_SNPs/reference.fa -o results/"$sample_id"/"$1"/1_SNPs/reference_snps.fa -V results/"$sample_id"/"$1"/1_SNPs/"$sample_id"_sorted.rmdup.grouped.filtered.snps.vcf


#mask bases
mapping "$1" 2_readdepth results/"$sample_id"/"$1"/1_SNPs/reference_snps.fa
java -jar "$GATKPATH"/GenomeAnalysisTK.jar -T HaplotypeCaller -ploidy 1 -R results/"$sample_id"/"$1"/2_readdepth/reference.fa -I results/"$sample_id"/"$1"/2_readdepth/"$sample_id"_sorted.rmdup.grouped.bam  -o results/"$sample_id"/"$1"/2_readdepth/"$sample_id"_sorted.rmdup.grouped.g.vcf.gz -ERC BP_RESOLUTION
zcat results/"$sample_id"/"$1"/2_readdepth/"$sample_id"_sorted.rmdup.grouped.g.vcf.gz  | vcffilter -g " DP  < 10 " | sed 's/<NON_REF>/N/'| awk '{if(substr($1, 1, 1)=="#") print ; else if ($4!="N" && $10!=".") print $1"\t"$2"\t"$3"\t"substr($4, 1, 1)"\tN\t"$6"\t"$7"\t"$8"\t"$9"\t"$10}' > results/"$sample_id"/"$1"/2_readdepth/"$sample_id"_sorted.rmdup.grouped.lowcov.vcf
java -jar "$GATKPATH"/GenomeAnalysisTK.jar -T FastaAlternateReferenceMaker -R results/"$sample_id"/"$1"/2_readdepth/reference.fa -o results/"$sample_id"/"$1"/2_readdepth/reference_snps_lowcov.fa -V results/"$sample_id"/"$1"/2_readdepth/"$sample_id"_sorted.rmdup.grouped.lowcov.vcf

python MLST/genomes/get_core_genes.py results/"$sample_id"/"$1"/2_readdepth/reference_snps_lowcov.fa "$sample_id" "$1" > results/"$sample_id"/"$1"/"$sample_id"_on_"$1".fasta

}

call_variants reference
call_variants ST-"$sequencetype"


#java -jar "$GATKPATH"/GenomeAnalysisTK.jar -T HaplotypeCaller -ploidy 1 -R results/"$sample_id"/1_mapping/reference.fa -I results/"$sample_id"/1_mapping/"$sample_id"_sorted.rmdup.grouped.bam  -o results/"$sample_id"/1_mapping/"$sample_id"_sorted.rmdup.grouped.g.vcf.gz -ERC BP_RESOLUTION


#java -jar "$GATKPATH"/GenomeAnalysisTK.jar -T FastaAlternateReferenceMaker -R reference.fa -o output.fasta  -V output.g.vcf.gz

#java -jar "$PICARDPATH"/picard.jar AddOrReplaceReadGroups I=run185-atcc-49775_sorted.rmdup.sorted.bam O=run185-atcc-49775_sorted.rmdup.sorted.group.bam RGID=4 RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=20

#https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_fasta_FastaAlternateReferenceMaker.php


#cat output.g.vcf | vcffilter -g " DP  < 10 " | sed 's/<NON_REF>/N/'
#GenotypeGVCFs
#zcat run185-atcc-49775_sorted.rmdup.grouped.g.vcf.gz  | vcffilter -f " DP  < 10 " | sed 's/<NON_REF>/N/'| awk '{if(substr($1, 1, 1)=="#") print ; else if ($10!=".") print $1"\t"$2"\t"$3"\t"substr($4, 1, 1)"\tN\t"$6"\t"$7"\t"$8"\t"$9"\t"$10}'

#vcftools --gzvcf run185-atcc-49775_sorted.rmdup.grouped.vcf.gz --remove-indels --recode --recode-INFO-all --stdout | vcffilter -f " DP  > 9 & AF > 0.9 "

