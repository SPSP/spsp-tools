Software	version 	notes
trimmomatic	0.38
bwa	0.7.17-r1188
pilon	1.23
VariantBam	b797b78
SAMTools	1.9
BCFTools	1.9
BEDTools	2.27.1
vcflib	7e3d806
seqtk	1.2-r94
SnpEff	4.3t	included in the dependencies

