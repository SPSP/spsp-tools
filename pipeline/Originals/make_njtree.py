#!/usr/bin/env python
import sys
from skbio import DistanceMatrix
from skbio.tree import nj
inputOptions = sys.argv[1:]
#usage genome strain_name reference
def main():
	strain2alleles={}
	with open(inputOptions[0],'r') as f:
		for raw_line in f:
			line=raw_line.replace("\n","").replace("\r","")
			strain=line.split("\t")[0]
			alleles=line.split("\t")[1:]
			alleles=[x if x.isdigit() else "" for x in alleles  ]
			strain2alleles[strain]=alleles

	strains=sorted(strain2alleles.keys())

	allel_matrix=[]
	for strain1 in strains:
		allel_line=[]
		for strain2 in strains:
			counter=0
			for allel1, allel2 in zip(strain2alleles[strain1], strain2alleles[strain2]):
				if allel1 != allel2 and allel1!= "" and allel2!= "" :
					counter+=1
			allel_line.append(counter)

		allel_matrix.append(allel_line)

	dm = DistanceMatrix(allel_matrix, strains)
	tree = nj(dm)
	newick_str = nj(dm, result_constructor=str)

	print(newick_str)

main()
