Bootstrap: docker
From: quay.io/biocontainers/metaphlan2:2.7.8--py27h24bf2e0_0


%environment
	# Section to define environment variables that will be available to the container at runtime.
	# We use it to define the name, version and release of the container.
	export CONTAINER_NAME="MetaPhlAn2"
	export CONTAINER_VERSION="2.0"
	export CONTAINER_RELEASE="1.biocontainers"


%help
####################################################################################################
### Container metadata:
### ******************
### Name:    MetaPhlAn2
### Version: 2.7.8
### Release: 1.biocontainers
### Summary: Metagenomic Phylogenetic Analysis
### Group:   Container
### License: MIT
### URL:     http://huttenhower.sph.harvard.edu/metaphlan2
###
###
### Description:
### ***********
### MetaPhlAn is a computational tool for profiling the composition of microbial
### communities (Bacteria, Archaea, Eukaryotes and Viruses) from metagenomic
### shotgun sequencing data with species level resolution. From version 2.0
### MetaPhlAn is also able to identify specific strains (in the not-so-frequent
### cases in which the sample contains a previously sequenced strains) and to
### track strains across samples for all species. MetaPhlAn relies on unique
### clade-specific marker genes identified from ~17,000 reference genomes
### (~13,500 bacterial and archaeal, ~3,500 viral, and ~110 eukaryotic), allowing:
###     - up to 25,000 reads-per-second (on one CPU) analysis speed (orders of
###       magnitude faster compared to existing methods)
###     - unambiguous taxonomic assignments as the MetaPhlAn markers are
###       clade-specific
###     - accurate estimation of organismal relative abundance (in terms of number
###       of cells rather than fraction of reads)
###     - species-level resolution for bacteria, archaea, eukaryotes and viruses
###     - extensive validation of the profiling accuracy on several synthetic
###       datasets and on thousands of real metagenomes.
###
###
### Running the container:
### *********************
### Available commands: metaphlan2.py, merge_metaphlan_tables.py, metaphlan_hclust_heatmap.py, metaphlan2krona.py, strainphlan.py
###
### To run any command in the container, replace <command to run> in command below.:
###   singularity exec /software/singularity/containers/Projects/SPSP/MetaPhlAn2-2.7.8-1.biocontainers.sif metaphlan2.py <options>
###
### To make a specific repository, such as "/scratch/cluster/monthly/<yourDirectory>", available
### inside the container add the option "--bind <directory>". Here is an example:
###   singularity exec --bind <directory> /software/singularity/containers/Projects/SPSP/MetaPhlAn2-2.7.8-1.biocontainers.sif metaphlan2.py <options>
###
### To run the default command for the container the command is the following. The default command
### generally displays the "help" section of the software installed in the container:
###   singularity run-help /software/singularity/containers/Projects/SPSP/MetaPhlAn2-2.7.8-1.biocontainers.sif
####################################################################################################



%runscript
	echo "### This is the container for $CONTAINER_NAME-$CONTAINER_VERSION-$CONTAINER_RELEASE."
	echo "### Help page for $CONTAINER_NAME:"
	metaphlan2.py --help
	exit 0


%test
	metaphlan2.py --help


%labels
	# Section to set custom metadata to be added to the container.
	AUTHOR sebastien.moretti@sib.swiss
	MAINTAINER Vital-IT SIB Swiss Institute of Bioinformatics


%post
	# $CONTAINER_NAME is already distributed as a docker container, so there is nothing more to install.

