Bootstrap: docker
From: quay.io/biocontainers/vcflib:1.0.0_rc1--h82df9c4_3


%environment
	# Section to define environment variables that will be available to the container at runtime.
	# We use it to define the name, version and release of the container.
	export CONTAINER_NAME="vcflib"
	export CONTAINER_VERSION="1.0.0_rc1"
	export CONTAINER_RELEASE="1.biocontainers"


%help
####################################################################################################
### Container metadata:
### ******************
### Name:    vcflib
### Version: 1.0.0_rc1
### Release: 1.biocontainers
### Summary: A C++ library for parsing and manipulating VCF files
### Group:   Container
### License: MIT
### URL:     https://github.com/vcflib/vcflib
###
###
### Description:
### ***********
### The Variant Call Format (VCF) is a flat-file, tab-delimited textual format intended
### to concisely describe reference-indexed variations between individuals. VCF provides
### a common interchange format for the description of variation in individuals and
### populations of samples, and has become the defacto standard reporting format for a
### wide array of genomic variant detectors.
###
### vcflib provides methods to manipulate and interpret sequence variation as it can be
### described by VCF. It is both:
###
###     an API for parsing and operating on records of genomic variation as it can be
###     described by the VCF format.
###     and a collection of command-line utilities for executing complex manipulations
###     on VCF files.
###
### The API itself provides a quick and extremely permissive method to read and write VCF
### files. Extensions and applications of the library provided in the included utilities
### (*.cpp) comprise the vast bulk of the library utility for most users.
###
###
### Running the container:
### *********************
### Available commands: abba-baba, bFst, bed2region, bgziptabix, dumpContigsFromHeader, genotypeSummary, hapLrt, LD, iHS, meltEHH, normalize-iHS, pFst, pVst, permuteGPAT++, permuteSmooth, plotBfst.R, plotHapLrt.R, plotHaplotypes.R, plotHaps, plotPfst.R, plotSmoothed.R, plotWCfst.R, plotXPEHH.R, plot_roc.r, popStats, segmentFst, segmentIhs, sequenceDiversity, smoother, vcf2bed.py, vcf2dag, vcf2fasta, vcf2sqlite.py, vcf2tsv, vcf_strip_extra_headers, vcfaddinfo, vcfafpath, vcfallelicprimitives, vcfaltcount, vcfannotate, vcfannotategenotypes, vcfbiallelic, vcfbreakmulti, vcfcat, vcfcheck, vcfclassify, vcfcleancomplex, vcfclearid, vcfclearinfo, vcfcombine, vcfcommonsamples, vcfcomplex, vcfcountalleles, vcfcreatemulti, vcfdistance, vcfecho, vcfentropy, vcfevenregions, vcffilter, vcffirstheader, vcffixup, vcfflatten, vcfgeno2alleles, vcfgeno2haplo, vcfgenosamplenames, vcfgenosummarize, vcfgenotypecompare, vcfgenotypes, vcfglbound, vcfglxgt, vcfgtcompare.sh, vcfhetcount, vcfhethomratio, vcfindelproximity, vcfindels, vcfindex, vcfinfo2qual, vcfinfosummarize, vcfintersect, vcfjoincalls, vcfkeepgeno, vcfkeepinfo, vcfkeepsamples, vcfleftalign, vcflength, vcfmultiallelic, vcfmultiway, vcfmultiwayscripts, vcfnobiallelicsnps, vcfnoindels, vcfnormalizesvs, vcfnosnps, vcfnull2ref, vcfnulldotslashdot, vcfnumalt, vcfoverlay, vcfparsealts, vcfplotaltdiscrepancy.r, vcfplotaltdiscrepancy.sh, vcfplotsitediscrepancy.r, vcfplottstv.sh, vcfprimers, vcfprintaltdiscrepancy.r, vcfprintaltdiscrepancy.sh, vcfqual2info, vcfqualfilter, vcfrandom, vcfrandomsample, vcfregionreduce, vcfregionreduce_and_cut, vcfregionreduce_pipe, vcfregionreduce_uncompressed, vcfremap, vcfremoveaberrantgenotypes, vcfremovenonATGC, vcfremovesamples, vcfroc, vcfsample2info, vcfsamplediff, vcfsamplenames, vcfsitesummarize, vcfsnps, vcfsort, vcfstats, vcfstreamsort, vcfuniq, vcfuniqalleles, vcfunphase, vcfvarstats, wcFst
###
### To run any command in the container, replace <command to run> in command below:
###   singularity exec /software/singularity/containers/Projects/SPSP/vcflib-1.0.0_rc1-1.biocontainers.sif <options>
###
### To make a specific repository, such as "/scratch/cluster/monthly/<yourDirectory>", available
### inside the container add the option "--bind <directory>". Here is an example:
###   singularity exec --bind <directory> /software/singularity/containers/Projects/SPSP/vcflib-1.0.0_rc1-1.biocontainers.sif <options>
###
### To run the default command for the container the command is the following. The default command
### generally displays the "help" section of the software installed in the container:
###   singularity run-help /software/singularity/containers/Projects/SPSP/vcflib-1.0.0_rc1-1.biocontainers.sif
####################################################################################################



%runscript
	echo "### This is the container for $CONTAINER_NAME-$CONTAINER_VERSION-$CONTAINER_RELEASE"
	echo "### Help page for $CONTAINER_NAME:"
	vcffilter
	exit 0


%test
	vcffilter


%labels
	# Section to set custom metadata to be added to the container.
	AUTHOR sebastien.moretti@sib.swiss
	MAINTAINER Vital-IT SIB Swiss Institute of Bioinformatics


%post
	# $CONTAINER_NAME is already distributed as a docker container, but this folder cannot be created

