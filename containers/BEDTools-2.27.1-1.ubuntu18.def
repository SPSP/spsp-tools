Bootstrap: docker
From: ubuntu:18.04


%environment
	# Section to define environment variables that will be available to the container at runtime.
	# We use it to define the name, version and release of the container.
	export CONTAINER_NAME="BEDTools"
	export CONTAINER_VERSION="2.27.1"
	export CONTAINER_RELEASE="1.ubuntu18"


%help
####################################################################################################
### Container metadata:
### ******************
### Name:    BEDTools
### Version: 2.27.1
### Release: 1.ubuntu18
### Summary: The swiss army knife for genome arithmetic
### Group:   Container
### License: MIT
### URL:     https://github.com/arq5x/bedtools2
###
###
### Description:
### ***********
### Collectively, the bedtools utilities are a swiss-army knife of tools for a
### wide-range of genomics analysis tasks. The most widely-used tools enable genome
### arithmetic: that is, set theory on the genome. For example, bedtools allows one
### to intersect, merge, count, complement, and shuffle genomic intervals from
### multiple files in widely-used genomic file formats such as BAM, BED, GFF/GTF,
### VCF.
### While each individual tool is designed to do a relatively simple task (e.g.,
### intersect two interval files), quite sophisticated analyses can be conducted by
### combining multiple bedtools operations on the UNIX command line.
###
###
### Running the container:
### *********************
### Available commands: bedtools, annotateBed, bamToBed, bamToFastq, bed12ToBed6, bedToBam, bedToIgv, bedpeToBam, closestBed, clusterBed, complementBed, coverageBed, expandCols, fastaFromBed, flankBed, genomeCoverageBed, getOverlap, groupBy, intersectBed, linksBed, mapBed, maskFastaFromBed, mergeBed, multiBamCov, multiIntersectBed, nucBed, pairToBed, pairToPair, randomBed, shiftBed, shuffleBed, slopBed, sortBed, subtractBed, tagBam, unionBedGraphs, windowBed, windowMaker
###
### To run any command in the container, replace <command to run> in command below:
###   singularity exec /software/singularity/containers/Projects/SPSP/BEDTools-2.27.1-1.ubuntu18.sif <command> <options>
###
### To make a specific repository, such as "/scratch/cluster/monthly/<yourDirectory>", available
### inside the container add the option "--bind <directory>". Here is an example:
###   singularity exec --bind <directory> /software/singularity/containers/Projects/SPSP/BEDTools-2.27.1-1.ubuntu18.sif <command> <options>
###
### To run the default command for the container the command is the following. The default command
### generally displays the "help" section of the software installed in the container:
###   singularity run-help /software/singularity/containers/Projects/SPSP/BEDTools-2.27.1-1.ubuntu18.sif
####################################################################################################



%runscript
	echo "### This is the container for $CONTAINER_NAME-$CONTAINER_VERSION-$CONTAINER_RELEASE"
	echo "### Help page for $CONTAINER_NAME:"
	bedtools -h
	exit 0


%labels
	# Section to set custom metadata to be added to the container.
	AUTHOR sebastien.moretti@sib.swiss
	MAINTAINER Vital-IT SIB Swiss Institute of Bioinformatics


%post
	# install software
	apt-get update -y
	export DEBIAN_FRONTEND=noninteractive
	apt-get install -y unzip wget gawk zlib1g-dev gcc g++ make python

	cd /usr/local/
	wget 'https://github.com/arq5x/bedtools2/releases/download/v2.27.1/bedtools-2.27.1.tar.gz'
	tar xvfz bedtools-2.27.1.tar.gz
	rm -f bedtools-2.27.1.tar.gz
	cd bedtools2/
	make
	make install
	make test || true

	# Cleaning
	## So try to remove manually useless stuff
	apt remove -y *-dev wget git gcc g++ make
	apt autoremove -y
	apt-get clean
	rm -rf /var/lib/apt/lists/*


%test
	bedtools -h || true

