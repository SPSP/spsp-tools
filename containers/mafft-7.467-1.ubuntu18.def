Bootstrap: docker
From: ubuntu:18.04


%environment
	# Section to define environment variables that will be available to the container at runtime.
	# We use it to define the name, version and release of the container.
	export CONTAINER_NAME="mafft"
	export CONTAINER_VERSION="7.467"
	export CONTAINER_RELEASE="1.ubuntu18"


%help
####################################################################################################
### Container metadata:
### ******************
### Name:    mafft
### Version: 7.467
### Release: 1.ubuntu18
### Summary: Multiple alignment program for amino acid or nucleotide sequences
### Group:   Container
### License: BSD + Extensions
### URL:     https://mafft.cbrc.jp/alignment/software/
###
###
### Description:
### ***********
### MAFFT is a multiple sequence alignment program for unix-like operating systems.
### It offers a range of multiple alignment methods, L-INS-i (accurate; for
### alignment of <~200 sequences), FFT-NS-2 (fast; for alignment of <~30,000
### sequences), etc.
###
###
### Running the container:
### *********************
### Available commands: mafft
###
### To run any command in the container, replace <command to run> in command below:
###   singularity exec /software/singularity/containers/Projects/SPSP/mafft-7.467-1.ubuntu18.sif mafft <options>
###
### To make a specific repository, such as "/scratch/cluster/monthly/<yourDirectory>", available
### inside the container add the option "--bind <directory>". Here is an example:
###   singularity exec --bind <directory> /software/singularity/containers/Projects/SPSP/mafft-7.467-1.ubuntu18.sif mafft <options>
###
### To run the default command for the container the command is the following. The default command
### generally displays the "help" section of the software installed in the container:
###   singularity run-help /software/singularity/containers/Projects/SPSP/mafft-7.467-1.ubuntu18.sif
####################################################################################################



%runscript
	echo "### This is the container for $CONTAINER_NAME-$CONTAINER_VERSION-$CONTAINER_RELEASE"
	echo "### Help page for $CONTAINER_NAME:"
	mafft --help
	exit 0


%labels
	# Section to set custom metadata to be added to the container.
	AUTHOR sebastien.moretti@sib.swiss
	MAINTAINER Vital-IT SIB Swiss Institute of Bioinformatics


%post
	# install software
	apt-get update -y
	export DEBIAN_FRONTEND=noninteractive
	apt-get install -y unzip wget gawk zlib1g-dev gcc g++ make

	cd /usr/local/
	wget 'https://mafft.cbrc.jp/alignment/software/mafft-7.467-with-extensions-src.tgz'
	tar xvfz mafft-7.467-with-extensions-src.tgz
	rm -f mafft-7.467-with-extensions-src.tgz
	cd mafft-7.467-with-extensions/core/
	make clean
	make
	make install
	cd ../extensions/
	make clean
	make
	make install

	# Cleaning
	## So try to remove manually useless stuff
	apt remove -y *-dev wget git gcc g++ make
	apt autoremove -y
	apt-get clean
	rm -rf /var/lib/apt/lists/*


%test
	mafft --help || true

