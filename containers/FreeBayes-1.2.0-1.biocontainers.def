Bootstrap: docker
From: quay.io/biocontainers/freebayes:1.2.0--py36h82df9c4_3


%environment
	# Section to define environment variables that will be available to the container at runtime.
	# We use it to define the name, version and release of the container.
	export CONTAINER_NAME="FreeBayes"
	export CONTAINER_VERSION="1.2.0"
	export CONTAINER_RELEASE="1.biocontainers"


%help
####################################################################################################
### Container metadata:
### ******************
### Name:    FreeBayes
### Version: 1.2.0
### Release: 1.biocontainers
### Summary: Bayesian haplotype-based genetic polymorphism discovery and genotyping
### Group:   Container
### License: MIT
### URL:     https://github.com/ekg/freebayes
###
###
### Description:
### ***********
### FreeBayes  is  a Bayesian  genetic  variant detector  designed  to find  small
### polymorphisms,  specifically  SNPs  (single-nucleotide polymorphisms),  indels
### (insertions and deletions), MNPs (multi-nucleotide polymorphisms), and complex
### events (composite insertion and substitution events) smaller  than the  length
### of a short-read sequencing alignment.
### FreeBayes is haplotype-based, in the sense that it calls variants based on the
### literal sequences of reads aligned to a particular target, not  their  precise
### alignment.  This model is a  straightforward generalization of  previous  ones
### (e.g.  PolyBayes,  samtools,  GATK) which detect or  report  variants based on
### alignments. This method  avoids one of the  core problems with alignment-based
### variant detection ---  that identical  sequences  may have  multiple  possible
### alignments.
### FreeBayes uses short-read alignments (BAM files with Phred+33  encoded quality
### scores, now standard) for any number of individuals from  a population  and  a
### reference genome (in FASTA format) to determine the most-likely combination of
### genotypes  for the population  at each position in the reference.  It  reports
### positions  which it finds  putatively polymorphic  in variant call  file (VCF)
### format.  It can also use an  input set of variants (VCF)  as a source of prior
### information, and a copy number variant map (BED)  to define non-uniform ploidy
### variation across the samples under analysis.
###
###
### Running the container:
### *********************
### Available commands: freebayes, freebayes-parallel, generate_freebayes_region_scripts.sh, coverage_to_regions.py, bamleftalign
###
### To run any command in the container, replace <command to run> in command below:
###   singularity exec /software/singularity/containers/Projects/SPSP/FreeBayes-1.2.0-1.biocontainers.sif mentalist freebayes <options>
###
### To make a specific repository, such as "/scratch/cluster/monthly/<yourDirectory>", available
### inside the container add the option "--bind <directory>". Here is an example:
###   singularity exec --bind <directory> /software/singularity/containers/Projects/SPSP/FreeBayes-1.2.0-1.biocontainers.sif freebayes <options>
###
### To run the default command for the container the command is the following. The default command
### generally displays the "help" section of the software installed in the container:
###   singularity run-help /software/singularity/containers/Projects/SPSP/FreeBayes-1.2.0-1.biocontainers.sif
####################################################################################################



%runscript
	echo "### This is the container for $CONTAINER_NAME-$CONTAINER_VERSION-$CONTAINER_RELEASE"
	echo "### Help page for $CONTAINER_NAME:"
	freebayes --help
	exit 0


%test
	freebayes --help


%labels
	# Section to set custom metadata to be added to the container.
	AUTHOR sebastien.moretti@sib.swiss
	MAINTAINER Vital-IT SIB Swiss Institute of Bioinformatics


%post
	# $CONTAINER_NAME is already distributed as a docker container, but this folder cannot be created

