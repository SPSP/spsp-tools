Bootstrap: docker
From: ubuntu:18.04


%environment
	# Section to define environment variables that will be available to the container at runtime.
	# We use it to define the name, version and release of the container.
	export CONTAINER_NAME="Trimmomatic"
	export CONTAINER_VERSION="0.38"
	export CONTAINER_RELEASE="1.ubuntu"


%help
####################################################################################################
### Container metadata:
### ******************
### Name:    Trimmomatic
### Version: 0.38
### Release: 1.ubuntu
### Summary: A flexible read trimming tool for Illumina NGS data
### Group:   Container
### License: GPLv3+
### URL:     http://www.usadellab.org/cms/?page=trimmomatic
###
###
### Description:
### ***********
### Trimmomatic performs a variety of useful trimming tasks for illumina paired-end
### and single ended data. The selection of trimming steps and their associated
### parameters are supplied on the command line.
###
### The current trimming steps are:
###
###    ILLUMINACLIP: Cut adapter and other illumina-specific sequences from the read.
###    SLIDINGWINDOW: Perform a sliding window trimming, cutting once the average
###    quality within the window falls below a threshold.
###    LEADING: Cut bases off the start of a read, if below a threshold quality
###    TRAILING: Cut bases off the end of a read, if below a threshold quality
###    CROP: Cut the read to a specified length
###    HEADCROP: Cut the specified number of bases from the start of the read
###    MINLEN: Drop the read if it is below a specified length
###    TOPHRED33: Convert quality scores to Phred-33
###    TOPHRED64: Convert quality scores to Phred-64
###
### It works with FASTQ (using phred + 33 or phred + 64 quality scores, depending on
### the Illumina pipeline used), either uncompressed or gzipped FASTQ. Use of gzip
### format is determined based on the .gz extension.
###
### For single-ended data, one input and one output file are specified, plus the
### processing steps. For paired-end data, two input files are specified, and 4 output
### files, 2 for the 'paired' output where both reads survived the processing, and 2
### for corresponding 'unpaired' output where a read survived, but the partner read did not.
###
###
### Running the container:
### *********************
### Available commands: trimmomatic
###
### To run any command in the container, replace <command to run> in command below:
###   singularity exec /software/singularity/containers/Projects/SPSP/Trimmomatic-0.38-1.ubuntu18.sif trimmomatic <command to run>
###
### To make a specific repository, such as "/scratch/cluster/monthly/<yourDirectory>", available
### inside the container add the option "--bind <directory>". Here is an example:
###   singularity exec --bind <directory> /software/singularity/containers/Projects/SPSP/Trimmomatic-0.38-1.ubuntu18.sif trimmomatic <command to run>
###
### To run the default command for the container the command is the following. The default command
### generally displays the "help" section of the software installed in the container:
###   singularity run-help /software/singularity/containers/Projects/SPSP/Trimmomatic-0.38-1.ubuntu18.sif
####################################################################################################



%labels
	# Section to set custom metadata to be added to the container.
	AUTHOR Sebastien.Moretti@sib.swiss
	MAINTAINER Vital-IT SIB Swiss Institute of Bioinformatics


%post
	# install software
	apt-get update -y
	apt-get install -y wget unzip openjdk-8-jre

	cd /usr/local/
	wget -c http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.38.zip
	unzip Trimmomatic-0.38.zip
	cd Trimmomatic-0.38/
	wget -c http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/TrimmomaticManual_V0.32.pdf
	cat << \EOF > trimmomatic
#!/bin/sh
java -jar /usr/local/Trimmomatic-0.38/trimmomatic-0.38.jar $@
EOF
	chmod 755 trimmomatic
	ln -s $PWD/trimmomatic /usr/local/bin/

	# Cleaning
	## So try to remove manually useless stuff
	apt remove -y *-dev wget git make gcc g++
	apt autoremove -y
	apt-get clean
	rm -rf /var/lib/apt/lists/*


%test
	trimmomatic -help


%runscript
	echo "### This is the container for $CONTAINER_NAME-$CONTAINER_VERSION-$CONTAINER_RELEASE"
	echo "### Help page for $CONTAINER_NAME:"
	trimmomatic -help
	exit 0

