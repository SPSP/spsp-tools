Bootstrap: docker
From: quay.io/biocontainers/bcftools:1.9--h4da6232_0
#Try to get the same commit than samtools to be sync


%environment
	# Section to define environment variables that will be available to the container at runtime.
	# We use it to define the name, version and release of the container.
	export CONTAINER_NAME="bcftools"
	export CONTAINER_VERSION="1.9"
	export CONTAINER_RELEASE="1.biocontainers"


%help
####################################################################################################
### Container metadata:
### ******************
### Name:    bcftools
### Version: 1.9
### Release: 1.biocontainers
### Summary: Utilities that manipulate variant calls in the variant call format (vcf) and its binary counterpart bcf
### Group:   Container
### License: MIT
### URL:     http://www.htslib.org/
###
###
### Description:
### ***********
### Bcftools is a set of utilities that manipulate variant calls in the variant
### call format (vcf) and its binary counterpart bcf. All commands work
### transparently with both vcfs and bcfs, both uncompressed and bgzf-compressed.
### Most commands accept vcf, bgzipped vcf and bcf with filetype detected
### automatically even when streaming from a pipe. Indexed vcf and bcf will work
### in all situations. Un-indexed vcf and bcf and streams will work in most, but
### not all situations.
###
###
### Running the container:
### *********************
### Available commands: bcftools
###
### To run any command in the container, replace <command to run> in command below:
###   singularity exec /software/singularity/containers/Projects/SPSP/bcftools-1.9-1.biocontainers.sif <options>
###
### To make a specific repository, such as "/scratch/cluster/monthly/<yourDirectory>", available
### inside the container add the option "--bind <directory>". Here is an example:
###   singularity exec --bind <directory> /software/singularity/containers/Projects/SPSP/bcftools-1.9-1.biocontainers.sif <options>
###
### To run the default command for the container the command is the following. The default command
### generally displays the "help" section of the software installed in the container:
###   singularity run-help /software/singularity/containers/Projects/SPSP/bcftools-1.9-1.biocontainers.sif
####################################################################################################



%runscript
	echo "### This is the container for $CONTAINER_NAME-$CONTAINER_VERSION-$CONTAINER_RELEASE"
	echo "### Help page for $CONTAINER_NAME:"
	bcftools --help
	exit 0


%test
	bcftools --help


%labels
	# Section to set custom metadata to be added to the container.
	AUTHOR sebastien.moretti@sib.swiss
	MAINTAINER Vital-IT SIB Swiss Institute of Bioinformatics


%post
	# $CONTAINER_NAME is already distributed as a docker container, but this folder cannot be created

