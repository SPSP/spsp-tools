# singularity recipes

Collection of singularity recipe files (.def) used to build containers that get deployed in the Vital-IT software stack.

Built containers are available in **/software/singularity/containers/Projects/SPSP/**

